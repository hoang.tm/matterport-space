

class Navigate3d{


    path;

    pathdots;

    graph;

    path_nolift;


    constructor(graph,segdist){
        console.log('Graph: ', graph);
        console.log('Segdist: ', segdist);

        this.path = createGraph();
        this.path_nolift = createGraph();

        this.graph = graph

  
        var dotcounter = 0;
        this.pathdots = [];
        graph.route.forEach(route=>{

            
    
            var startnode = graph.node[route.start]
            var endnode = graph.node[route.end]

            //console.log(startnode);
    
            var startpos = startnode.position
            var endpos = endnode.position
    
            var counter = Math.floor(route.weight/segdist)
    
            var diffx = (startpos.x - endpos.x)/counter
            var diffy = (startpos.y - endpos.y)/counter
            var diffz = (startpos.z - endpos.z)/counter
    
    
            var tmpdots = [];
    
            tmpdots.push(startnode)
            this.pathdots.push(startnode)
    
            for (var i=1;i<counter;i++){
    
                var point = {};
    
                point.x = startpos.x-i*diffx
                point.y = startpos.y-i*diffy
                point.z = startpos.z-i*diffz
               
                //console.log(point)
    
                var dotlabel = `dot_${dotcounter}`;
    
                var dotdetail = {};
                dotdetail.label = dotlabel;
                dotdetail.position = point;
    
                tmpdots.push(dotdetail);
                this.pathdots.push(dotdetail);
    
                dotcounter++;
            }
    
            tmpdots.push(endnode)
            this.pathdots.push(endnode)
            
    
                 for (var j=0;j<tmpdots.length-1;j++){

                    //console.log(startnode)

                    if (startnode.label.substring(0,3) != "lif" && endnode.label.substring(0,3) != "lif"){

                        this.path_nolift.addLink(tmpdots[j].label,tmpdots[j+1].label,{weight:segdist})

                        this.path_nolift.addLink(tmpdots[j+1].label,tmpdots[j].label,{weight:segdist})
                    }

                  this.path.addLink(tmpdots[j].label,tmpdots[j+1].label,{weight:segdist})

                  this.path.addLink(tmpdots[j+1].label,tmpdots[j].label,{weight:segdist})
                }
    
        })


        console.log(this.path)
        console.log(this.path_nolift)
    
        

    }



    planroute(startpos,endpos,isLift){

        var currentdot = this.findNeareastPathDot(startpos)
        console.log(currentdot)

        var destinationdot = this.findNeareastPathDot(endpos)
        console.log(destinationdot)


        var isOriented = false
        //if (this.graph.config.hasOwnProperty("isBidirectional")){
        //    isOriented = this.graph.config.isBidirectional
        //}


        var path = this.path;

        if (!isLift){
            path = this.path_nolift
        }


        let pathFinder = ngraphPath.aStar(path,{
            distance(fromNode,toNode,link){

                return link.data.weight;
            },oriented:true
        })

        var route = pathFinder.find(currentdot.label,destinationdot.label);

        route =  route.reverse();

        var startdot = {}
        startdot.dotype = 'dot'
        startdot.position = startpos

        var routeWithCoord = [];

        counter = 0;

        route.forEach(node=>{
            //console.log(node.id)
            var result = jQuery.grep(this.pathdots, function (n, i) {
                return (n.label == node.id);
            });
            var dot = result[0]

            var clonedot = Object.assign({},dot);
           var height = dot.position.y + 0.3

           var dotobj = {}

           var dottype = 'dot';


             if (counter == 0 || counter == route.length-1){
                dottype = 'dot'

            }
            else if (node.id.substring(0,4) ==  "tag_"){
                dottype = "node"
            }
            else if (node.id.substring(0,4) ==  "lif_"){
                dottype = "lift"
            }



           clonedot.position = {x:dot.position.x,y:height,z:dot.position.z}

           dotobj.position = clonedot.position
           dotobj.dottype = dottype



            routeWithCoord.push(dotobj);

            counter++


        })

        var enddot = {}
        enddot.dottype = 'dot'
        enddot.position = endpos

        //routeWithCoord.push(enddot);


        console.log(routeWithCoord)



        // remove all dot within the list 


            var liftindex = [];

            var counter = 0;

            routeWithCoord.forEach(dot=>{

                if (dot.dottype == "lift"){

                    liftindex.push(counter)

                }

                counter++;

            })


            console.log(liftindex)

            if (liftindex.length == 2){

                var firstArray = routeWithCoord.slice(0,liftindex[0]+1)
                var secondArray = routeWithCoord.slice(liftindex[1])

                routeWithCoord = firstArray.concat(secondArray)

            }


        ////    


        console.log(routeWithCoord);

        var positions = [];

        var positions_trim = [];


        var counter = 0;
        
        routeWithCoord.forEach(dot=>{


            if (counter == 0 || counter == routeWithCoord.length-1){
                positions_trim.push(dot.position)
            }
            else if (dot.dottype ==  "node" || dot.dotype == "lift"){
                
                positions_trim.push(dot.position)
            }


            positions.push(dot.position)

            counter++

        })


        


        console.log()





        // recalibrate  end dot

        if (positions.length > 2){

            var lastdot = positions[positions.length-1];
            var last2dot = positions[positions.length-2];

            var newdot = {x:lastdot.x,y:last2dot.y,z:lastdot.z}
            

            positions[positions.length-1] = newdot;
            positions_trim[positions_trim.length-1] = newdot
        }
        

        

        var route = {};

        route.full = positions



        if (positions.length < 5){
            route.trim = positions
        }
        else{
            route.trim = positions_trim
        }
        

        console.log(route);
        
        return route;
    }




    
    findNeareastPathDot(position){

        var nearestdot = this.pathdots[0]

        var nearestdist = 9999999999;
       
        this.pathdots.forEach(dot =>{

            var dotpos =  dot.position

            //var dist = Math.abs(dotpos.x-position.x) + Math.abs(dotpos.z-position.z) +Math.abs(dotpos.y-position.y)

            var dist = Math.pow((dotpos.x-position.x),2) + Math.pow((dotpos.y-position.y),2) +  Math.pow((dotpos.z-position.z),2)

            if (dist < nearestdist){

                nearestdist = dist;
                nearestdot = dot;
            }

        })


        return nearestdot


    }


    static makeStepSmaller(route,numberOfSubStep){

        console.log("make step")

        var newroute = [];

        for (var counter = 0; counter<route.full.length-1;counter++){

            var start = route.full[counter]
            var end = route.full[counter+1];

            var diffx = (end.x - start.x)/numberOfSubStep
            var diffy = (end.y - start.y)/numberOfSubStep
            var diffz = (end.z - start.z)/numberOfSubStep


            for (var i=0;i<numberOfSubStep;i++){

                var newstep = {};

                newstep.x = start.x + i*diffx
                newstep.y = start.y + i*diffy
                newstep.z = start.z + i*diffz


               // console.log(newstep)
                newroute.push(newstep)



            }

        }



        return newroute;


    }


    static findNearestPathDotWithPath(route,position){

       // console.log(route)
        var nearestdot = route[0]

        var nearestdist = 9999999999;
       
        route.forEach(dot =>{

            //console.log(dot)

            var dotpos =  dot

            //var dist = Math.abs(dotpos.x-position.x) + Math.abs(dotpos.z-position.z) +Math.abs(dotpos.y-position.y)

            var dist = Math.pow((dotpos.x-position.x),2) + Math.pow((dotpos.y-position.y),2) +  Math.pow((dotpos.z-position.z),2)

            if (dist < nearestdist){

                nearestdist = dist;
                nearestdot = dot;
            }

        })


        return {position:nearestdot}       
    }



    static distanceComparison(coord1,coord2){

        //console.log(coord1,coord2)
        return Math.pow((coord1.x-coord2.x),2) + Math.pow((coord1.y-coord2.y),2) +  Math.pow((coord1.z-coord2.z),2)
    }

    static distanceComparisonPlain(coord1,coord2){

        return Math.abs(coord1.x-coord2.x) + Math.abs(coord1.y-coord2.y)  + Math.abs(coord1.z-coord2.z)
    }

    static findNearestDotInPathIndex(currentpos,path){

        var sdist = 99999999999
        var sindex = 0;

        var index = 0;

        path.forEach(dot =>{

            //console.log(currentpos,dot)

            var dist = this.distanceComparison(currentpos,dot);

            //console.log(index,dist)

            if (dist < sdist){

                sdist = dist;
                sindex = index;

            }



            index++
        })


        return sindex;

    }



    static findNearestDotInPathDetail(currentpos,path,segdist){

        var res = {};

        var index = this.findNearestDotInPathIndex(currentpos,path);

        var dot = path[index];

        var distFromUser = Math.sqrt(this.distanceComparison(currentpos,dot));


        res.index = index;
        res.dot = dot;
        res.dist = distFromUser;
        res.distFromStart = segdist*index;
        res.distFromDestination = segdist*(path.length-index);

        return res;

    }


    static rotateWhenStart(gridpath){

        var startpoint = gridpath[0];

        var pointindex = 3;
    
        if (gridpath.length < 4){
            pointindex = gridpath.length-1
        }
    
        var nextpoint = gridpath[pointindex];

        console.log(startpoint,nextpoint);

        var bearing = this.findRotateAngle(startpoint,nextpoint)

        console.log(bearing)

        return new Promise((resolve,reject)=>{

            _mpSdk.Camera.setRotation({x:-20,y:bearing.y},3)
            .then((e)=>{

        
                resolve("finish rotate")
        
            })
        })

    }


    static rotateWhenStartWithIndex(gridpath,index){


        var startindex = Math.max(0,index-2)

        var startpoint = gridpath[startindex];

        var nextindex = index+5;

        if (nextindex > gridpath.length-5){
            nextindex = gridpath.length-1;
        }
    

        var nextpoint = gridpath[nextindex];

        //console.log(nextpoint)

        var bearing =  this.findRotateAngle(startpoint,nextpoint);

        return new Promise((resolve,reject)=>{

            _mpSdk.Camera.setRotation({x:-20,y:bearing},3)
            .then((e)=>{

        
                resolve("finish rotate")
        
            })
        



        })
    




    }



    static findNearestSweep(sweeps,coordinate){

        var shortestdistance = 99999999999;

        var neareastsweep = {};

        $.each(sweeps,(index,sweep)=>{


            //console.log(sweep);

            var coord1 = sweep.position
            var coord2 = coordinate;

            var dist = this.distanceComparisonPlain(coord1,coord2)

            //console.log(sweep.uuid,dist)

            if (dist < shortestdistance){

                //console.log(sweep)
                neareastsweep = sweep;
                shortestdistance = dist;
            }

        })

        console.log(neareastsweep)
        return neareastsweep;

    }



    static findRotateAngle(startpoint,nextpoint){

        var bearing;
        if (startpoint.x == nextpoint.x){
    
            if (startpoint.z > nextpoint.z){
    
                bearing = -90
            }
            else{
                bearing = 90
            }
    
        }
        else{

          var ratio =   (startpoint.z-nextpoint.z)/(startpoint.x-nextpoint.x)
           bearing  = Math.atan(ratio)
    
           bearing = bearing *180/Math.PI 
        }
    
        var newangle;
        
    
        newangle = -bearing - 90 
    
    
        if (nextpoint.x < startpoint.x){
    
            newangle = -bearing +90
        }

        var angle = {};
        angle.y = newangle;
        angle.x = nextpoint.y - startpoint.y;

        return angle;

    }

 

    static findNearestPathDot(sweep,pathInCoordinate){

        var position = sweep.position;
       // var currSweepCoord = {x:position.x,y:position.z}

       var currSweepCoord = position

        var nearestDist = 99999999;
        var nearestIndex;

        var nearestDot = {}
        var nearestCoord = {};


        // comment

        $.each(pathInCoordinate,(index,dot)=>{


            var dist = this.distanceComparison(dot,currSweepCoord)
            if (dist < nearestDist){

                nearestIndex = index;
                nearestCoord = dot;

                nearestDist = dist;

            }

        })

        
        nearestDot.index = nearestIndex;
        nearestDot.coordinate = nearestCoord;
        nearestDot.distance = Math.sqrt(nearestDist);

        return nearestDot;
    }


     findPathInSweepList(sweeps,pathInCoordinate){

        var routeSweepPath = [];


       // console.log(sweeps);


        var lastAddSweepDistanceFromEnd = 99999999999;

        var endpoint = pathInCoordinate[pathInCoordinate.length-1];
        $.each(pathInCoordinate,(index,coord)=>{


            var nearestDist = 99999999
            var nearestSweep = "";

            $.each(sweeps,(index,sweep)=>{

                //console.log(sweep)

                var sweepcoord = sweep.position;
                var dist = this.constructor.distanceComparison(coord,sweepcoord)

                if (dist < nearestDist){
                    nearestDist = dist;
                    nearestSweep = sweep
                }

            })

            //console.log(nearestDist);
            routeSweepPath.push(nearestSweep) 

            // if (routeSweepPath.indexOf(nearestSweep) == -1 && nearestDist < 10){
            //     //console.log(nearestSweep)


            //         if (routeSweepPath.length > 0){

            //             var lastsweep = routeSweepPath[routeSweepPath.length-1];

            //             var distBetweenTwoPoints = this.distanceComparison(lastsweep.position,nearestSweep.position)

            //             //console.log("distance ",distBetweenTwoPoints)

            //             //if (distBetweenTwoPoints <15){
            //                 if (distBetweenTwoPoints <3){

            //                 console.log("too close , not add",distBetweenTwoPoints)
            //                 return;
            //             }
            //         }

            //         // check distance 

            //        if  (this.distanceComparison(nearestSweep.position,endpoint) < lastAddSweepDistanceFromEnd){

  
            //             lastAddSweepDistanceFromEnd = this.distanceComparison(nearestSweep.position,endpoint)


            //             routeSweepPath.push(nearestSweep)                  
            //        }
            //        else{
            //            console.log("sweep is farthest than the pervious sweep from the end point")
            //        }




                
            // }
        })


        console.log(routeSweepPath);


        ////// remove same sweep 


        var removeDuplicateRoute = [];

        routeSweepPath.forEach(sweep=>{

            const found = removeDuplicateRoute.find(ele => ele.uuid == sweep.uuid)

           // console.log(found)

            if (found == undefined){

               // console.log("add pt")
                removeDuplicateRoute.push(sweep)
            }



        })

        routeSweepPath = removeDuplicateRoute

        console.log(routeSweepPath);




        var thersholdcompress = 30

        //////// compress sweep 

        var divisor = Math.ceil(routeSweepPath.length/thersholdcompress)
        console.log(`divisor ${divisor}`);

        console.log(this.graph)

        if (routeSweepPath.length > thersholdcompress){

            var compressSweepPath = [];

            var counter = 0;
            routeSweepPath.forEach(sweep=>{


                var ignore = false;

                // if height level > 0.3 , keep sweep in compress

                if (compressSweepPath.length>0){

                    var lastsweep = compressSweepPath[compressSweepPath.length-1];

                    if (Math.abs(lastsweep.position.y - sweep.position.y) > 0.3){

                        ignore = true;
                        compressSweepPath.push(sweep)
                    }

                }



                // if sweep near node or lift , keep 

                if (!ignore){


                    Object.keys(this.graph.node).forEach(key=>{

                        var node = this.graph.node[key]
                        if (this.constructor.distanceComparisonPlain(node.position,sweep.position)<2){

                            console.log("near node , so ignore");
                            ignore = true;
                            

                        }

                    })

                }



                if (!ignore){

                

                    if (counter/divisor == Math.floor(counter/divisor)){

                        compressSweepPath.push(sweep)
                    }

                    counter++;
                }
            })

            routeSweepPath = compressSweepPath;
        }



        return routeSweepPath;


    }


    static findPathInSweepList(sweeps,pathInCoordinate){

        var routeSweepPath = [];


       // console.log(sweeps);


        var lastAddSweepDistanceFromEnd = 99999999999;

        var endpoint = pathInCoordinate[pathInCoordinate.length-1];
        $.each(pathInCoordinate,(index,coord)=>{


            var nearestDist = 99999999
            var nearestSweep = "";

            $.each(sweeps,(index,sweep)=>{

                //console.log(sweep)

                var sweepcoord = sweep.position;
                var dist = this.distanceComparison(coord,sweepcoord)

                if (dist < nearestDist){
                    nearestDist = dist;
                    nearestSweep = sweep
                }

            })

            //console.log(nearestDist);
            routeSweepPath.push(nearestSweep) 

            // if (routeSweepPath.indexOf(nearestSweep) == -1 && nearestDist < 10){
            //     //console.log(nearestSweep)


            //         if (routeSweepPath.length > 0){

            //             var lastsweep = routeSweepPath[routeSweepPath.length-1];

            //             var distBetweenTwoPoints = this.distanceComparison(lastsweep.position,nearestSweep.position)

            //             //console.log("distance ",distBetweenTwoPoints)

            //             //if (distBetweenTwoPoints <15){
            //                 if (distBetweenTwoPoints <3){

            //                 console.log("too close , not add",distBetweenTwoPoints)
            //                 return;
            //             }
            //         }

            //         // check distance 

            //        if  (this.distanceComparison(nearestSweep.position,endpoint) < lastAddSweepDistanceFromEnd){

  
            //             lastAddSweepDistanceFromEnd = this.distanceComparison(nearestSweep.position,endpoint)


            //             routeSweepPath.push(nearestSweep)                  
            //        }
            //        else{
            //            console.log("sweep is farthest than the pervious sweep from the end point")
            //        }




                
            // }
        })


        console.log(routeSweepPath);


        ////// remove same sweep 


        var removeDuplicateRoute = [];

        routeSweepPath.forEach(sweep=>{

            const found = removeDuplicateRoute.find(ele => ele.uuid == sweep.uuid)

           // console.log(found)

            if (found == undefined){

               // console.log("add pt")
                removeDuplicateRoute.push(sweep)
            }



        })

        routeSweepPath = removeDuplicateRoute

        console.log(routeSweepPath);




        var thersholdcompress = 30

        //////// compress sweep 

        var divisor = Math.ceil(routeSweepPath.length/thersholdcompress)
        console.log(`divisor ${divisor}`);

        console.log(this.graph)

        if (routeSweepPath.length > thersholdcompress){

            var compressSweepPath = [];

            var counter = 0;
            routeSweepPath.forEach(sweep=>{


                var ignore = false;

                // if height level > 0.3 , keep sweep in compress

                if (compressSweepPath.length>0){

                    var lastsweep = compressSweepPath[compressSweepPath.length-1];

                    if (Math.abs(lastsweep.position.y - sweep.position.y) > 0.3){

                        ignore = true;
                        compressSweepPath.push(sweep)
                    }

                }






                if (!ignore){

                

                    if (counter/divisor == Math.floor(counter/divisor)){

                        compressSweepPath.push(sweep)
                    }

                    counter++;
                }
            })

            routeSweepPath = compressSweepPath;
        }



        return routeSweepPath;


    }


    static checkOrientation(pose,pathInCoordinate,offset){

        //console.log(pose)
       // console.log(pathInCoordinate)

       var isForward = true

        var nearestCoord = this.findNearestPathDot(pose,pathInCoordinate);

        //console.log(nearestCoord)

        var index = nearestCoord.index

        var startpoint = pathInCoordinate[index]


        var indexOffset = 4

        var nextindex = index+indexOffset;

        if (nextindex > pathInCoordinate.length-indexOffset){
            nextindex = pathInCoordinate.length-1
        }
    

        var nextpoint = pathInCoordinate[nextindex];


        var direction = this.findRotateAngle(startpoint,nextpoint);
        var rotation = pose.rotation.y ;

        var answer = 0;

        if (rotation < 0){

            rotation = 360 + rotation
        }

        if (direction < 0){

            direction = 360 + direction;
        }

        if (rotation < offset && direction >= 360 - offset){
            answer = Math.abs(rotation + (360 - direction))
        }       
        else if (direction < offset && rotation >= 360 - offset){
            answer = Math.abs(direction + (360 - rotation))
        }
        else{
            answer = Math.abs(rotation - direction)
        }


        if ( answer < offset ){
            isForward = true;
        }
        else{
            isForward = false;

        }

       
        
        return isForward



    }


    static  drawRoute(route,isUIShow,isShowLine){

        var positions = route.full;

        
        if (positions.length > 0 ){



            var lastpoint = Math.min(5,positions.length-1)


            var pointa = {x:positions[0].x,y:positions[0].z}

            var nextptindex = 5;

            if (positions.length<5){
                nextptindex = positions.length - 1;
            }
            console.log(nextptindex)
            var pointb = {x:positions[nextptindex].x,y:positions[nextptindex].z}

            var bearing = RouteMethod.findRotateAngle(pointa,pointb)


            _mpSdk.Camera.setRotation({x:0,y:bearing},3)
            .then((e)=>{

               
               
               
                var buttonstr = {cancel:{
                    label:'Walk',
                    className:'btn btn-secondary',
                    callback:function(){
                        console.log('walk')
                    }
                },
                sweepmove:{
                    label:'Auto Move',
                    className:'btn btn-secondary',
                    callback:function(){

                        

                        console.log(_allSweepArray)
                        console.log(route.full)

                        var sweeppath = Navigate3d.findPathInSweepList(_allSweepArray,route.full);

                        console.log(sweeppath)

                        _isAutoSweepMove = true;
                        sweepmove(sweeppath,route.full,0)
                        


                    }

                },
                fly:{
                    label:'Direct Fly',
                    className:'btn btn-secondary',
                    callback:function(){
                        directfly();
                    }
                }}


                if (typeof robotlead === 'function'){


                    var buttonstr = {cancel:{
                        label:'Walk',
                        className:'btn btn-secondary',
                        callback:function(){
                            console.log('walk')
                        }
                    },
                    sweepmove:{
                        label:'Auto Move',
                        className:'btn btn-secondary',
                        callback:function(){
    
                            
    
                            console.log(_allSweepArray)
                            console.log(route.full)
    
                            var sweeppath = this.findPathInSweepList(_allSweepArray,route.full);
    
                            console.log(sweeppath)
    
                            _isAutoSweepMove = true;
                            sweepmove(sweeppath,route.full,0)
                            
    
    
                        }
    
                    },
                    fly:{
                        label:'Direct Fly',
                        className:'btn btn-secondary',
                        callback:function(){
                            directfly();
                        }
                    },
                    robot:{
                        label : "Robot",
                        className : 'btn btn-secondary',
                        callback:function(){

                            
                            robotlead(route);

                            removeAllLine()
                        }
                    }
                }


                }

                if (isUIShow){


                     _mpSdk.Floor.showAll()

                    bootbox.dialog({
                        title:"Choose the way to go",
                        message:' ',

                        buttons:buttonstr


                    })


                }



                if (isShowLine){

                    drawline(route,0)
    
                }
                else{
    
                    positions.forEach(dot=>{
    
                        var tag = {}
    
                        console.log(dot)
    
                        tag.anchorPosition = {x:parseFloat(dot.x),y:parseFloat(dot.y),z:parseFloat(dot.z)}
                        tag.stemVector = zeroStemVector
                        tag.label = dot.label
                        tag.description = "pathfind"
                        tag.color = {r:0.5,g:0.5,b:0.5}
                        
        
                        _mpSdk.Mattertag.add(tag)
                        .then(sids=>{
                            var sid = sids[0]
                            _mpSdk.Mattertag.preventAction(sid,{navigating:true,opening:true});
                            _mpSdk.Mattertag.editOpacity(sid,0.8)
                        })
    
    
                    })
    
                }
            })


        }
    

    }



}