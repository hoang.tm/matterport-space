let browserId;
let sdk;
var ct;

function listenContentLoad(browserid){
  browserId = browserid;
  const iframe = document.getElementById('showcase-iframe');

  if(iframe!=null){
      connectSdk();
  }else{
      sleep(5000).then(() => { listenContentLoad(browserId); });
  }
}

function sleep(ms) {
return new Promise(resolve => setTimeout(resolve, ms));
}

async function connectSdk() {
// connect the sdk; log an error and stop if there were any connection issues
try {
  connectShowcase();
} catch (e) {
  console.error(e);
}
}

async function connectShowcase(){
const showcase = document.getElementById('showcase-iframe');
const showcaseWindow = await showcase.contentWindow;

setTimeout(async function (){
  sdk = await showcaseWindow.MP_SDK.connect(showcaseWindow, 'gznrh4n499h4qhewiitm1hidc', '');

  console.log('WELCOME TO MATTERPORT');
  console.log('SDK: ', sdk);
  initLight();
  initLaser();
  createObjectWalking("https://cdn.jsdelivr.net/gh/duchieupham/matterport@main/vinai-dj2-walking.glb");
  createGif();
  initJackDoor();
  ct = new Commontools();

  // var mattertags = [{
  //   label: 'Tag 1',
  //   description: 'This is tag 1',
  //   media          : {
  //     type : sdk.Mattertag.MediaType.PHOTO,
  //     src  :
  //       'https://images.unsplash.com/photo-1513366208864-87536b8bd7b4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
  //   },
  //   anchorPosition: { x: 0, y: 0, z: 0},
  //   stemVector: { x: 0, y: 0, z: 0}
  // },{
  //   label: 'Tag 2',
  //   media: {
  //     type: sdk.Mattertag.MediaType.VIDEO,
  //     src: 'https://www.youtube.com/embed/DVOQpp0qS5U',
  //   },
  //   anchorPosition: { x: 1, y: 0, z: 0 },
  //   stemVector: { x: 0, y: 0.5, z: 0}
  // }];
  // await sdk.Mattertag.add(mattertags).then(function(mattertagIds) {
  //   console.log("Matter tags ids: " + mattertagIds);
  //   sdk.Mattertag.preventAction(mattertagIds,{navigating:true,opening:true});
  // });

  // sdk.Camera.pose.subscribe(function (pose) {
  //   // Changes to the Camera pose have occurred.
  //   console.log('Current position is ', pose.position);
  //   console.log('Rotation angle is ', pose.rotation);
  //   console.log('Sweep UUID is ', pose.sweep);
  //   console.log('View mode is ', pose.mode);
  // });


  var tagHandler = new TagHandler();
  tagHandler.addTag({
    anchorPosition: { x: 0, y: 0, z: 0},
    stemVector: { x: 0, y: 0.5, z: 0},
  });
}, 1000);
}

async function initLight(){

const [ sceneObject ] = await sdk.Scene.createObjects(1);

const lightNode = sceneObject.addNode();

const directionalLightComponet = lightNode.addComponent('mp.directionalLight', {
  color: { r: 0.7, g: 0.7, b: 0.7 },
});
lightNode.addComponent('mp.ambientLight', {
  intensity: 0.5,
  color: { r: 1.0, g: 1.0, b: 1.0 },
});

sceneObject.addInputPath(directionalLightComponet, 'intensity', 'ambientIntensity');
sceneObject.start();
    
}

function createObjectWalking(url){
  var obj = JSON.parse(data);
  new Walking(obj, url);
}

var urlObject;
const data = '{"edge":{"tag_0":["tag_1"],"tag_1":["tag_2"],"tag_2":["tag_3"],"tag_3":["tag_4"],"tag_4":["tag_5"],"tag_5":["tag_6"],"tag_6":["tag_7"],"tag_7":["tag_8"],"tag_8":["tag_9"],"tag_9":["tag_10"],"tag_10":["tag_11"],"tag_11":["tag_12"],"tag_12":["tag_13"],"tag_13":["tag_14"],"tag_14":["tag_15"],"tag_15":["tag_16"],"tag_16":["tag_17"],"tag_17":["tag_18"],"tag_18":["tag_0"]},"node":{"tag_0":{"position":{"x":-9.987982749938965,"y":0.09,"z":-2.5423858636708827},"floorIndex":0,"label":"tag_0"},"tag_1":{"position":{"x":-4.794413089752197,"y":-0.02,"z":-3.334177255630493},"floorIndex":0,"label":"tag_1"},"tag_2":{"position":{"x":-1.6169832944869995,"y":0,"z":-2.768167734146118},"floorIndex":0,"label":"tag_2"},"tag_3":{"position":{"x":3.1049084663391113,"y":0.01,"z":-3.2828681468963623},"floorIndex":0,"label":"tag_3"},"tag_4":{"position":{"x":5.233036518096924,"y":0.09,"z":-2.0382375717163086},"floorIndex":0,"label":"tag_4"},"tag_5":{"position":{"x":4.615228176116943,"y":0.09,"z":-0.07181860506534576},"floorIndex":0,"label":"tag_5"},"tag_6":{"position":{"x":4.459445476531982,"y":0.1,"z":2.830822229385376},"floorIndex":0,"label":"tag_6"},"tag_7":{"position":{"x":8.211172103881836,"y":0.1,"z":2.6326961517333984},"floorIndex":0,"label":"tag_7"},"tag_8":{"position":{"x":10.168307304382324,"y":0.1,"z":2.7117230892181396},"floorIndex":0,"label":"tag_8"},"tag_9":{"position":{"x":10.630560874938965,"y":0.09,"z":0.5689769387245178},"floorIndex":0,"label":"tag_9"},"tag_10":{"position":{"x":10.568989753723145,"y":0.09,"z":-1.6269643306732178},"floorIndex":0,"label":"tag_10"},"tag_11":{"position":{"x":6.964844226837158,"y":0.09,"z":-1.4862850904464722},"floorIndex":0,"label":"tag_11"},"tag_12":{"position":{"x": 5.233036518096924,"y":0.09,"z":-2.0382375717163086},"floorIndex":0,"label":"tag_12"},"tag_13":{"position":{"x":3.1049084663391113,"y":0.02,"z":-3.2828681468963623},"floorIndex":0,"label":"tag_13"},"tag_14":{"position":{"x":0.06633119285106659,"y":0.001,"z":-0.8547800183296204},"floorIndex":0,"label":"tag_14"},"tag_15":{"position":{"x": 0.7149236798286438,"y":-0.003,"z":1.9419901371002197},"floorIndex":0,"label":"tag_15"},"tag_16":{"position":{"x":-0.5606495141983032,"y":-0.015,"z":3.076383113861084},"floorIndex":0,"label":"tag_16"},"tag_17":{"position":{"x":-5.094160556793213,"y":-0.018,"z":3.497300386428833},"floorIndex":0,"label":"tag_17"},"tag_18":{"position":{"x":-7.735805988311768,"y":0.12,"z":2.094252347946167},"floorIndex":0,"label":"tag_18"}},"route":[{"start":"tag_0","end":"tag_1","weight":0.8743118312850332},{"start":"tag_1","end":"tag_2","weight":3.7608170411177575},{"start":"tag_2","end":"tag_3","weight":2.486488658749331},{"start":"tag_3","end":"tag_4","weight":2.395741936236084},{"start":"tag_4","end":"tag_5","weight":3.570183953216865},{"start":"tag_5","end":"tag_6","weight":2.623857214898033},{"start":"tag_6","end":"tag_7","weight":2.0368629190732968},{"start":"tag_7","end":"tag_8","weight":3.3672431099293996},{"start":"tag_8","end":"tag_9","weight":1.1468453757302428},{"start":"tag_9","end":"tag_10","weight":4.217583226626331},{"start":"tag_10","end":"tag_11","weight":4.255392255770029},{"start":"tag_11","end":"tag_12","weight":2.6899605621270717},{"start":"tag_12","end":"tag_13","weight":4.913005000429337},{"start":"tag_13","end":"tag_14","weight":2.0896222514141383},{"start":"tag_14","end":"tag_15","weight":4.513020534449613},{"start":"tag_15","end":"tag_16","weight":1.5800374580454977},{"start":"tag_16","end":"tag_17","weight":2.3650404307734627},{"start":"tag_17","end":"tag_18","weight":2.2486881590298484},{"start":"tag_18","end":"tag_0","weight":1.8743501420991784}],"islift":false}';

function stopComponentDJ(url){
  Object3D.prototype.finishNotification();
  createModel(url);
}

function stopComponentWalking(url){
  Walking.prototype.finishNotification();
  createObjectWalking(url);
}

// Create 3D Object
async function createModel(url){
  let typeComponent;
  urlObject = url;

  new Object3D();

// const [ sceneObject ] = await sdk.Scene.createObjects(1);

// const modelNode = sceneObject.addNode();

// typeComponent = checkTypeComponent(type);
// modelNode.obj3D.scale.set(scale, scale, scale);
// modelNode.obj3D.position.set(positionX, positionY, positionZ); 
// modelNode.obj3D.rotation.set(0, Math.PI / 2, 0);
// await createObjectFactory();
// // Store the fbx component since we will need to adjust it in the next step.
// const fbxComponent = modelNode.addComponent('objectFactory');

// console.log('Properties object: ', fbxComponent);
// console.log('Properties model node: ', modelNode);

// sceneObject.start();

// if(isRorate == true){
//   rorateObject(modelNode);
// }

// if(type == 'gltfLoader'){
//   modelNode.obj3D.rotation.y -= 1;
// }
}

// Rorate 3D object
function rorateObject(modelNode){
const tick = function() {
  requestAnimationFrame(tick);
  modelNode.obj3D.rotation.y += 0.02;
}
tick();
}

// Check type component
function checkTypeComponent(type){
if(type == 'gltfLoader'){
  return sdk.Scene.Component.GLTF_LOADER;
}else if(type == 'objLoader'){
  return sdk.Scene.Component.OBJ_LOADER;
}else if(type == 'daeLoader'){
  return sdk.Scene.Component.DAE_LOADER;
}else if(type == 'fbxLoader'){
  return sdk.Scene.Component.FBX_LOADER;
} 
}

var dataPos = '[{"x": -1.7776111364364624, "y": 0.4, "z": 2.9653427600860596},{"x": -1.5873128175735474, "y": 0.4, "z": -1.3585586547851562},{"x": -3.2250428199768066, "y": 0.4, "z": -0.979781448841095},{"x": -3.095548629760742, "y": 0.4, "z": 2.706770181655884},{"x": -4.119585990905762, "y": 0.9, "z": 1.5777889490127563},{"x": -1.6926259994506836, "y": 0.4, "z": 1.3664237260818481},{"x": -2.179327964782715, "y": 0.4, "z": -3.2604169845581055},{"x": -3.2652127742767334, "y": 0.4, "z": -3.180114984512329},{"x": 0.09429296851158142, "y": 1, "z": -4.125218391418457},{"x": 0.9179532527923584, "y": 0.4, "z": -2.212691068649292},{"x": 0.7149236798286438, "y": 0.4, "z": 1.9419901371002197},{"x": 1.8537302017211914, "y": 0.6, "z": 3.1344523429870605}]';

// Create object gif
function createGif(){
  new Multi_gifuser(JSON.parse(dataPos));
}

function initLaser(){
  const laserData = '[{"x": -5.654860668182373, "y": 2.91968994140625, "z": -2.827944326400757},{"x": -5.494160556793213, "y": 2.91968994140625, "z": 2.93300386428833}]';

  new Laser(JSON.parse(laserData));
}

function initJackDoor(){
  var param = '{"filename": "gif_dance-06.png", "scale": {"x": 1, "y": 1, "z": 1}, "position": {"x": -7.5962417678036624, "y": 0.6394819720827437, "z": -4.85219839607208}, "rotation":{"_x": 0, "_y":0, "_z": 0}, "url": "https://3d2api.discoverfeed.info/version-test/api/1.1/wf/push", "urlImage": "https://cdn.jsdelivr.net/gh/duchieupham/matterport@main/gif_dance-06.png", "id": "01", "message": "JACKDOOR", "holdonposyoffset": 0.7}';
  new Door(JSON.parse(param));
}

/** Embed Image */
async function embedImage(url, scale, positionX, positionY, positionZ, msg){

  await sdk.Scene.registerComponents([
    {
      name:'imageEmbed',
      factory: imageEmbedFatory,
    }
  ]);

  const [ sceneObject ] = await sdk.Scene.createObjects(1);

  const imageNode = sceneObject.addNode();

  imageNode.obj3D.scale.set(scale, scale, scale); 

  imageNode.obj3D.position.set(positionX, positionY, positionZ); 

  // imageNode.obj3D.rotation.set(rotation._x, rotation._y, rotation._z);

  imageNode.obj3D.name = url.substring(11, url.length);

  const imageComponent = imageNode.addComponent('imageEmbed');

  imageComponent.inputs.src = url;

  imageComponent.onInit = function (){

    const THREE = this.context.three;
    // var geometry = new THREE.PlaneGeometry(2.06, 3.5);
    var geometry = new THREE.PlaneGeometry(2.4, 3.05);

    const texture  = new THREE.TextureLoader().load(imageComponent.inputs.src);

    texture.minFilter = THREE.LinearFilter;

    var material = new THREE.MeshBasicMaterial({
      map: texture,
      color: 0xffffff,
      side: THREE.DoubleSide,
      transparent: true,
    });
    material.map.needsUpdate = true;

    var tplane = new THREE.Mesh(geometry, material);

    tplane.name = url.substring(11, url.length);
    imageComponent.outputs.objectRoot = tplane;
    imageComponent.outputs.collider = tplane;

    texture.format = THREE.RGBAFormat;
    texture.needsUpdate = true;

    imageNode.obj3D.rotation.y -= 1;
  }

  imageComponent.events= (_a = {},
    _a["INTERACTION.CLICK"] = true,
    _a);
  imageComponent.onEvent = function (type, data){
    if(type == 'INTERACTION.CLICK'){
      window.postMessage({
        'message': 'OPEN IFRAME WALKING'
      }, '*');
      embedImageClicked();
    }
  };

  sceneObject.start();
}

function imageEmbedFatory(){
  return new ImageEmbed();
}

class ImageEmbed{
  constructor(){
    this.inputs = {
      src: null,
    }

    this.outputs = {
      texture: null,
    };

    this.onEvent = function (){};
    this.onInit = function (){}
  }
}
/** End Embed Image */

/**Embed image clicked */
async function embedImageClicked(posX, posY, posZ, isObjectWalking){

var isObjectWalking = isObjectWalking || false;

await registerClicked();

const [ sceneObject ] = await sdk.Scene.createObjects(1);

const imageNode = sceneObject.addNode();

imageNode.obj3D.scale.set(0.15, 0.15, 0.15); 

imageNode.obj3D.position.set(posX, posY, posZ); 

// imageNode.obj3D.rotation.set(rotation._x, rotation._y, rotation._z);

imageNode.obj3D.name = 'clicked-gif';

const imageComponent = imageNode.addComponent('imageClicked');
if(isObjectWalking){
  imageComponent.inputs.src = 'https://cdn.jsdelivr.net/gh/duchieupham/matterport@main/hold_on.png';
}else{
  imageComponent.inputs.src = 'https://cdn.jsdelivr.net/gh/duchieupham/matterport@main/clicked-gif.png';
}

imageComponent.onInit = function (){

  const THREE = this.context.three;
  // var geometry = new THREE.PlaneGeometry(2.06, 3.5);
  var geometry = new THREE.PlaneGeometry(2.4, 3.05);

  const texture  = new THREE.TextureLoader().load(imageComponent.inputs.src);

  texture.minFilter = THREE.LinearFilter;

  var material = new THREE.MeshBasicMaterial({
    map: texture,
    color: 0xffffff,
    side: THREE.DoubleSide,
    transparent: true,
  });
  material.map.needsUpdate = true;

  var tplane = new THREE.Mesh(geometry, material);

  tplane.name = 'clicked-gif';
  imageComponent.outputs.objectRoot = tplane;
  imageComponent.outputs.collider = tplane;

  texture.format = THREE.RGBAFormat;
  texture.needsUpdate = true;

  // imageNode.obj3D.rotation.y -= 1;
}

sceneObject.start();

var _this = this;
sdk.Camera.pose.subscribe(function (e) {
  var now_rotate = (e.rotation.y / 180) * Math.PI;
  imageNode.obj3D.rotation.set(0, now_rotate, 0);
});

setTimeout(() => sceneObject.stop(), 3000);
}

async function registerClicked(){
await sdk.Scene.registerComponents([
  {
    name:'imageClicked',
    factory: imageClickedFatory,
  }
]);
}

function imageClickedFatory(){
return new ImageClicked();
}

class ImageClicked{
constructor(){
  this.inputs = {
    src: null,
  }

  this.outputs = {
    texture: null,
  };
}
}

class TagHandler{
  addTag(tag){
      console.log("-----tag in add tag: " + tag);
      var self = this;
      var position =  tag.anchorPosition;
      // var color = tag.color;
      var stemVector = tag.stemVector;
    
      sdk.Mattertag.add([{
          anchorPosition: position,
          stemVector: stemVector,
          // color: color,
          stemVisible:tag.stemVisible

      }]).then(sids=>{
          var sid = sids[0]
          self.createOwnBillboard(sid,tag.sid)
          console.log("sid: " + sid);
          console.log("tag id: " + tag.sid);

      })
  }
  createOwnBillboard(sid,old_sid){
    try{
      console.log("createOwnBillboard " + sid);
      var self = this;
      var w = window.innerWidth;
      var h = window.innerHeight;
      var _isMobile = false;
      if(_isMobile){
          w = w * 0.7988;
          h = h * 0.30859;
      }else{
          /*w = w * 0.1988;
          h = h * 0.30859;*/
          w = "450";
          h = "350";
      }
      
      var youtube_id = '';
      var _sub_tag_list = this._sub_tag_list;
      var _tag_tn;
      // youtube_id = _sub_tag_list[old_sid].media['src'].split("/")[3];
      youtube_id = "https://www.youtube.com/watch?v=020g-0hhCAU";
      if(youtube_id.split("watch?v=").length > 1){
          youtube_id = youtube_id.split("watch?v=")[1];
          youtube_id = youtube_id.split("&")[0]
          youtube_id = youtube_id.split("?")[0]
      }
    
      var src = "";
     
      
          // if(_sub_tag_list[old_sid].media['src'].split("vimeo").length > 1){
          // _tag_tn = `https://vumbnail.com/${youtube_id}.jpg`;
          // src = "https://player.vimeo.com/video/"+youtube_id;
      
          // }else{
          console.log('youtube');
          _tag_tn = `https://img.youtube.com/vi/${youtube_id}/0.jpg`;
          src = "https://www.youtube.com/embed/"+youtube_id;
          // }
    
           
      
    
    
      var htmlToInject = `<style>
      @import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
      /* width */
      ::-webkit-scrollbar {
        width: 4px;
      }
      
      /* Track */
      ::-webkit-scrollbar-track {
        background: rgba(0,0,0,0); 
      }
       
      /* Handle */
      ::-webkit-scrollbar-thumb {
        background: rgba(136, 136, 136, 0); 
      }
      
      /* Handle on hover */
      ::-webkit-scrollbar-thumb:hover {
        background: #555; 
      }

      html:hover > ::-webkit-scrollbar-thumb{
          background: rgba(136, 136, 136, 0.4); 
      }

      .tag_body:hover > ::-webkit-scrollbar-thumb{
          background: rgba(136, 136, 136, 0.4); 
      }
      </style>
      <div class=tag_body>`;
      // var title_h = h * 0.23; 
      
      // if(_isMobile){
      //     var title_margin = "5px";
      //     var img_w = w * 0.85;
      //     var img_h = h * 0.78;
      // }else{
      //     var title_margin = "5px";
      //     var img_w = w * 0.9;
      //     var img_h = h * 0.7;
      // }
      // var pD = "";
      // var pLink = "";

      htmlToInject += `<p style="size:16px;font-family:'Roboto', sans-serif;color: #fff;font-weight: 700;white-space: pre-wrap;margin:0 0 0 0;max-height:300px;overflow: auto;">Xinchao<br>`
      htmlToInject += `</p>`;
      htmlToInject += `<video controls autoplay width="400" height="300" src="https://cdn.jsdelivr.net/gh/duchieupham/matterport@main/Baby%20Shark%20-%20CoComelon%20Nursery%20Rhymes%20%26%20Kids%20Songs.mp4" type="video/mp4"></video>`;
      // htmlToInject += '<iframe sandbox="allow-same-origin allow-scripts allow-popups allow-forms" width="400" height="300" src="https://www.youtube.com/embed/020g-0hhCAU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'

   
     
      // htmlToInject += `<p style='margin:0px;text-align: center;'><img id=video_tn src='${_vn_asset_path}subassets/play.png' style='width: 50px;position: absolute;top: 50%;left: calc(50% - 25px);'></img><img id=video_tn style="max-width:${img_w}px;max-height:${img_h}px;margin:auto;" src="${_tag_tn}"></img></p>`
      htmlToInject += `</div>`;
      // htmlToInject += `<script>
      // var video_tn = document.getElementById("video_tn"); 
      // video_tn.addEventListener("click", function () { 
      // window.send("buttonClick", "${src}");
      // });`;
      // // if(pLink.length > 0){
      // //     // htmlToInject += `var link_url = document.getElementById("${old_sid}"); ;
      // //     // link_url.addEventListener("click", function () { 
      // //     //     // window.send("${old_sid}", "${pLink}");
      // //     // });`;
      // // }
      
      // htmlToInject += `</script>`;
     
    
        sdk.Mattertag.injectHTML(sid, htmlToInject,{windowPath:'', size: {
          w: w,
          h: h,
        }} )
        .then(function (messenger) {
    
          // console.log(messenger)
    
          //   messenger.on("buttonClick", function (buttonId) {
          //     console.log('clicked button with id:', buttonId);
          //     if(!$('.tag_div').length){
          //         self.iframe(buttonId)
                
          //     }
          //     });
              // messenger.on(old_sid, function (urlLink) {
              //     console.log('clicked url:', urlLink);
              //     window.open(urlLink, '_blank');
              // });
          });
    
          // $.each(_mattertags,function(k,v){
          //     if(v.sid == old_sid){
          //         v.sid = sid;
          //     }
          // });
    
          // _sub_tag_list[sid] = _sub_tag_list[old_sid];
          // delete _sub_tag_list[old_sid];
          // sdk.Mattertag.remove(old_sid);
    
    }catch(e){
      console.log("Error at createOwnBillboard: " + e);
    }
   
  }
}

var nodeObj;
var Object3D = /** @class */ (function () {
  function Object3D() {
      var _this = this;
      this.isStop = false;
      this.filename = "https://cdn.jsdelivr.net/gh/duchieupham/matterport@main/vinai-dj2-walking.glb";
      console.log("Object3D");
      this.chaturl ='';
      obj3DModel = this;
      this.walkmodels = [];
      this.speed = 200;
      this.numberOfModels = 1;
      this.modelposition = { x: -1.6169832944869995, y: 0, z: -2.768167734146118};
      this.modelrotation = { _x: 0, _y: 0, _z: 0, _order: 'XYZ' };
      this.modelscale = 0.8;
      this.mixers = [];
      this.initChatIframe();
      Promise.all([
          sdk.Scene.register('objectFactory', this.animationFactory)
      ]).
          then(function () {
          console.log("animation register done");
          for (var i = 0; i < _this.numberOfModels; i++) {
              _this.loadmodel();
          }
      });
  }
  Object3D.prototype.initChatIframe = function () {
      // var _this = this;
      // var node = document.createElement('div');
      // var html = "\n        \n        <style>\n            #chatiframe{\n\n                position:relative;\n\n                width:100%;\n                height:90%;\n                \n                margin: 0 auto;\n                display:inline-block;\n\n\n            }\n\n\n            #closeChatIframeBtn{\n\n                position:relative;\n                float:right;\n                padding:5px;\n                //background-color:grey;\n                width:40px;\n                height:40px;\n                color:white;\n                display:inline-block;\n\n\n            }\n            #chatiframewrapper{\n                position:absolute;\n                width:80%;\n                height:80%;\n                top:50%;\n                left:10px;\n                transform:translate(0%,-50%);\n                background-color:rgba(0,0,0,0.5);\n                display:none;\n                z-index:9999999;\n                max-width:400px;\n            }\n\n\n        </style>\n        \n            <div id=chatiframewrapper>\n            <div id=closeChatIframeBtn>X</div>\n            <iframe id=chatiframe src=" + this.chaturl + "></iframe>\n            \n            </div>\n\n        ";
      // node.innerHTML = html;
      // document.body.appendChild(node);
      // $("#closeChatIframeBtn").click(function (e) {
      //     _this.closeChatIframe();
      // });
  };
  Object3D.prototype.loadmodel = function () {
      return __awaiter(this, void 0, void 0, function () {
          var _this = this;
          return __generator(this, function (_a) {
              switch (_a.label) {
                  case 0: return [4 /*yield*/, sdk.Scene.createNode()
                          .then(function (node) {
                          node.obj3D.scale.set(_this.modelscale, _this.modelscale, _this.modelscale);
                          node.obj3D.position.set(_this.modelposition.x, _this.modelposition.y, _this.modelposition.z);
                          node.obj3D.rotation.set(0, Math.PI / 2, 0);
                          node.addComponent('objectFactory');
                          nodeObj = node;
                          node.start();
                          console.log("model done");
                          console.log(node);
                          _this.walkmodels.push(node);
                      })];
                  case 1:
                      _a.sent();
                      return [2 /*return*/];
              }
          });
      });
  };
  Object3D.prototype.animationFactory = function () {
      return new objectFactory();
  };
  Object3D.prototype.finishNotification = function () {
      console.log("finish component");
      nodeObj.stop();
  };
  Object3D.prototype.update = function () {
      if (!this.isStop) {
          requestAnimationFrame(this.update.bind(this));
          var dt_1 = this.clock.getDelta();
          this.mixers.forEach(function (mixer) {
              //console.log(mixer)
              mixer.update(dt_1);
          });
      }
  };
  Object3D.prototype.openChatIframe = function () {
      $("#chatiframewrapper").css("display", "block");
  };
  Object3D.prototype.closeChatIframe = function () {
      var _this = this;
      $("#chatiframewrapper").css("display", "none");
      this.isStop = false;
      this.walkmodels.forEach(function (node) {
          _this.startwalk(node);
      });
      this.update();
  };
  return Object3D;
}());

function objectFactory() {
  this.onInit = function () {
      var _a;
      var _this = this;
      var THREE = this.context.three;
      console.log(THREE);
      obj3DModel.clock = new THREE.Clock();
      var loader = new THREE.GLTFLoader();
      var filename = obj3DModel.filename;
      console.log('URL UPDATE: ', urlObject);
      loader.load(urlObject, function (fbx) {
          console.log("load finish");
          console.log("fbx done");
          console.log(fbx);
          var mixer = new THREE.AnimationMixer(fbx.scene);
          var walkanim = fbx.animations[0];
          var action = mixer.clipAction(walkanim);
          action.clampWhenFinished = true;
          console.log('Walk Anim: ', walkanim);
          obj3DModel.mixers.push(mixer);
          action.play();
          action.timeScale = 0.8;
          obj3DModel.update();
          _this.outputs.objectRoot = fbx.scene;
          _this.outputs.collider = fbx.scene;
          fbx.scene.traverse(function (child) {
              //console.log(child)
              if (child.material) {
                  child.material.metalness = 0;
              }
          });
      });
      this.events = (_a = {},
          _a["INTERACTION.CLICK"] = true,
          _a["INTERACTION.HOVER"] = true,
          _a);
      this.onEvent = function (type, data) {
          if (type == "INTERACTION.CLICK") {
              // walk.isStop = true;
              // walk.openChatIframe();

              // window.postMessage('Clicked DJ', '*');
              embedImageClicked(obj3DModel.modelposition.x, obj3DModel.modelposition.y + 1.5, obj3DModel.modelposition.z);
          }
      };
  };}