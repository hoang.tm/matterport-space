var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};

var Laser = /** @class */ (function () {
    function Laser(laserdata) {
        this.laserNode = [];
        this.numberOfRay = 20;
        this.boxDim = 0.01;
        this.rayLength = 20;
        this.opacity = 0.4;
        this.timeForChangeLaserColorinSeconds = 3;
        this.colors = ["#7CFC00", "#ffff00", "#0000ff", "#ff0000"];
        this.laserdata = laserdata;
        console.log("laser");
        console.log(laserdata);
        laser = this;
        this.initLaser();
    }
    Laser.prototype.animate = function () {
        requestAnimationFrame(this.animate.bind(this));
        this.laserNode.forEach(function (node) {
            //console.log(node)
            node.obj3D.rotation.y += 0.01;
        });
    };
    Laser.prototype.initLaser = function () {
        var _this = this;
        Promise.all([
            sdk.Scene.register('laserbox', this.laserFactory)
        ]).
            then(function () {
            // console.log("init laser done")
            _this.build_line();
            _this.animate();
            _this.changeColor(0);
            //this.subscribeCameraChange()
        });
    };
    Laser.prototype.subscribeCameraChange = function () {
        sdk.Camera.pose.subscribe(function (e) {
            console.log(this.laserNode);
            if (this.laserNode.length > 0) {
                if (e.mode == "mode.dollhouse" || e.mode == "mode.floorplan") {
                    this.laserNode.forEach(function (node) {
                        node.obj3D.visible = false;
                    });
                }
            }
        });
    };
    Laser.prototype.changeColor = function (index) {
        var _this = this;
        index++;
        if (index > this.colors.length - 1) {
            index = 0;
        }
        var color = this.colors[index];
        if (this.laserNode.length > 0) {
            this.laserNode.forEach(function (node) {
                var lines = node.obj3D.children[0].children;
                lines.forEach(function (line) {
                    line.material.color.set(color);
                    // console.log(line.material)
                });
            });
        }
        //console.log(color)
        setTimeout(function () {
            _this.changeColor(index);
        }, this.timeForChangeLaserColorinSeconds * 1000);
    };
    Laser.prototype.build_line = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.laserdata.forEach(function (source) {
                            sdk.Scene.createNode()
                                .then(function (node) {
                                node.addComponent('laserbox');
                                node.start();
                                _this.laserNode.push(node);
                                node.obj3D.position.set(source.x, source.y, source.z);
                                console.log("node done");
                                console.log(node);
                            });
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Laser.prototype.laserFactory = function () {
        return new this.laserbox(function () {
            console.log("laser factory done");
            //sub_init_ui();
        });
    };
    return Laser;
}());
function laserbox() {
    this.onInit = function () {
        var THREE = this.context.three;
        //console.log(laser.laserLines)
        var THREE = this.context.three;
        //new THREE.WebGLRenderer.setPixelRatio(window.devicePixelRatio * 1.5);
        var renderer = new THREE.WebGLRenderer({ antialias: true });
        var material = new THREE.MeshStandardMaterial({ antialias: true, color: 0x00ff00, wireframe: false, transparent: true });
        material.transparent = true;
        material.opacity = 0.5;
        console.log(material);
        var ballgeo = new THREE.SphereGeometry(0.01, 10, 10);
        var ball = new THREE.Mesh(ballgeo, material);
        for (var i = 0; i < laser.numberOfRay; i++) {
            var linegeo = new THREE.BoxBufferGeometry(laser.rayLength, laser.boxDim, laser.boxDim, 10, 10, 10);
            linegeo.applyMatrix4(new THREE.Matrix4().makeTranslation(laser.rayLength / 2, 0, 0));
            var line = new THREE.Mesh(linegeo, material);
            // var line.position.set(0,0,0)
            var randomX = 0, randomY = 0, randomZ = 0;
            randomY = ((Math.random() * 360) - 180) * Math.PI / 180;
            randomZ = ((Math.random() * 180) - 180) * Math.PI / 180;
            line.rotation.set(randomX, randomY, randomZ);
            //console.log("line")
            //console.log(line)
            ball.add(line);
        }
        console.log(ball);
        this.outputs.objectRoot = ball;
    };
    this.inputs = {
        visible: false
    };
}
