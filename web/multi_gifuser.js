var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var gifPosition, indexFile;
var posX, posY, posZ, tmpPosX, tmpPosY, tmpPosZ;
var Multi_gifuser = /** @class */ (function () {
    function Multi_gifuser( pos) {
        gifPosition =pos;

        var _this = this;
        this.dancernodes = [];
        this.diamondnodes = [];
        this.numberOfDancers = 13;
        this.events = {};
        this.recommendlist = [];
        this.avatarkeygif = {};
        this.isLoadGif = false;
        this.gifpath = "../vn_assets/subassets/octagon/giffigure/avatar-d_7";
        this.gifdancerpath = "../vn_assets/subassets/octagon/";
        this.actions = {};
        this.maxNumberOfPosition = 12;
        this.refreshtime = 300000; // test 10 seconds
        giffigure = this;
        this.spriteMixer = SpriteMixer();
        console.log(this.gifpath);
        // console.log(param);
        // this.param = param;
        this.positions = this.getDancerPosition();
        console.log(this.positions);
        Promise.all([
            sdk.Scene.register('gif_a', this.gifFactory),
            // sdk.Scene.register('diamond', this.diamondFactory)
        ]).
            then(function () {
            // _this.downloadChatRecommend();
            _this.loadGif();
            console.log("register gif done");
        });
        this.listenMapChange();
    }
    // Multi_gifuser.prototype.downloadChatRecommend = function () {
    //     var _this = this;
    //     var browserId = '';
    //     if (ct.getUrlParameter('browserId')) {
    //         console.log("exist brosweri id");
    //         browserId = ct.getUrlParameter('browserId');
    //     }
    //     var cb = {};
    //     cb.browserId = browserId;
    //     var url = this.param.bubbleapiobj.chatlist_url;
    //     console.log(url);
    //     var istemp = false;
    //     if (istemp) {
    //         var tmpurl = this.gifdancerpath + "/jack_img/dummy_chatrecommend.txt";
    //         url = tmpurl;
    //     }
    //     $.post(url, cb, function (data) {
    //         console.log(data);
    //         if (istemp) {
    //             data = JSON.parse(data);
    //         }
    //         //var obj = JSON.parse(data)
    //         _this.recommendlist = data.response.users;
    //         _this.recommendlist = _this.recommendlist.splice(0, _this.maxNumberOfPosition);
    //         console.log(_this.recommendlist);
    //         if (!_this.isLoadGif) {
    //             _this.isLoadGif = true;
    //             _this.loadGif();
    //         }
    //         _this.loadDiamond();
    //     });
    // };
    Multi_gifuser.prototype.listenMapChange = function () {
        var _this = this;
        sdk.Camera.pose.subscribe(function (e) {
            _this.dancernodes.forEach(function (node) {
                var now_rotate = (e.rotation.y / 180) * Math.PI;
                node.obj3D.rotation.set(0, now_rotate, 0);
            });
            _this.diamondnodes.forEach(function (node) {
                var now_rotate = (e.rotation.y / 180) * Math.PI;
                node.obj3D.rotation.set(0, now_rotate, 0);
            });
        });
    };
    Multi_gifuser.prototype.getDancerPosition = function () {
        // console.log(this.param.scenedata.gifposition);
        var pos = [];
        // for (var key in this.param.scenedata.gifposition) {
            // pos.push({x: posX, y: posY, z: posZ});
            // pos.push({x: posX + 0.7, y: posY, z: posZ+ 1});
            // pos.push({x: posX - 0.7, y: posY, z: posZ- 1});
        // }
        gifPosition.forEach(function (position){
            pos.push({x: position.x, y: position.y, z: position.z});
        })
        return pos;
    };
    // getDancerPosition(numofitem ){
    //     var newarray = []
    //     var navpath =  new Navigate3d(this.param.path,1)
    //     console.log(navpath);
    //     var  navpath_copy = navpath.pathdots;
    //     for (var i = 0 ; i< numofitem; i++){
    //         var idx = Math.floor(Math.random() * navpath_copy.length);
    //         newarray.push(navpath_copy[idx].position);
    //         navpath_copy.splice(idx, 1);
    //     }
    //     console.log(newarray)
    //     return newarray;
    // }
    // Multi_gifuser.prototype.loadDiamond = function () {
    //     return __awaiter(this, void 0, void 0, function () {
    //         var counter;
    //         var _this = this;
    //         return __generator(this, function (_a) {
    //             switch (_a.label) {
    //                 case 0:
    //                     counter = 0;
    //                     return [4 /*yield*/, this.recommendlist.forEach(function (profile) {
    //                             //console.log(this.counter)
    //                             _mpSdk.Scene.createNode()
    //                                 .then(function (node) {
    //                                 _this.chatrecommondcounter = counter;
    //                                 var scale = 0.4;
    //                                 var diamondposyoffset = 1.15;
    //                                 var position = _this.param.scenedata.gifposition[counter + 1];
    //                                 console.log("diamond position");
    //                                 console.log(position);
    //                                 node.obj3D.scale.set(scale, scale, scale);
    //                                 if (_this.param.scenedata.diamondposyoffset) {
    //                                     diamondposyoffset = _this.param.scenedata.diamondposyoffset;
    //                                 }
    //                                 node.obj3D.position.set(position.x, position.y + diamondposyoffset, position.z);
    //                                 node.addComponent('diamond');
    //                                 node.obj3D.name = "diamond_" + counter;
    //                                 var now_rotate = (_pose.rotation.y / 180) * Math.PI;
    //                                 node.obj3D.rotation.set(0, now_rotate, 0);
    //                                 node.start();
    //                                 _this.diamondnodes.push(node);
    //                                 counter++;
    //                                 if (counter == _this.recommendlist.length) {
    //                                     setTimeout(function () {
    //                                         console.log("timeout refresh");
    //                                         giffigure.diamondnodes.forEach(function (node) {
    //                                             node.stop();
    //                                         });
    //                                         giffigure.downloadChatRecommend();
    //                                     }, _this.refreshtime);
    //                                 }
    //                             });
    //                         })];
    //                 case 1:
    //                     _a.sent();
    //                     return [2 /*return*/];
    //             }
    //         });
    //     });
    // };
    Multi_gifuser.prototype.loadGif = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.counter = 1;
                        this.positions = this.positions.splice(0, this.maxNumberOfPosition);
                        return [4 /*yield*/, this.positions.forEach(function (element) {
                                sdk.Scene.createNode().then(function (node) {
                                    indexFile = _this.counter;
                                    var scale = 0.3;
                                    var gifdancerposyoffset = 0.6;
                                    // console.log(_this.param.scenedata);
                                    // if (_this.param.scenedata.gifdancerscale) {
                                    //     scale = _this.param.scenedata.gifdancerscale;
                                    // }
                                    // if (_this.param.scenedata.gifdancerposyoffset) {
                                    //     gifdancerposyoffset = _this.param.scenedata.gifdancerposyoffset;
                                    // }
                                    node.obj3D.scale.set(scale, scale, scale);
                                    node.obj3D.position.set(element.x, element.y, element.z);
                                    tmpPosX = element.x;
                                    tmpPosY = element.y;
                                    tmpPosZ = element.z;
                                    node.addComponent('gif_a');
                                    node.obj3D.name = "gif_" + _this.counter;
                                    // var now_rotate = (_pose.rotation.y / 180) * Math.PI;
                                    node.obj3D.rotation.set(0, 0, 0);
                                    node.start();
                                    _this.dancernodes.push(node);
                                    _this.counter++;
                                });
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Multi_gifuser.prototype.loop = function () {
        requestAnimationFrame(this.loop.bind(this));
        this.delta = this.clock.getDelta();
        this.spriteMixer.update(this.delta);
    };
    Multi_gifuser.prototype.gifFactory = function () {
        return new gif_a();
    };
    // Multi_gifuser.prototype.diamondFactory = function () {
    //     return new diamond();
    // };
    Multi_gifuser.prototype.gifdanceraction = function (data) {
        var _this = this;
        console.log(data);
        var position = data.collider.parent.position;
        var dname = data.collider.parent.name;
        console.log(dname);
        var profileid = dname.split("_")[1];
        var profile = this.recommendlist[profileid];
        console.log(profile);
        console.log(position);
        console.log("gif action ");
        console.log("dispatch");
        var pm = {};
        pm.position = position;
        pm.profile = profile;
        var pmstring = JSON.stringify(pm);
        // var param = {};
        // param.profile = pmstring;
        // // make icon 
        // var url = '../controller/createJackTag.php';
        // $.post(url, param, function (obj) {
        //     console.log(obj);
        //     _this.dispatch("notification", pm);
        // });
    };
    ///////
    Multi_gifuser.prototype.dispatch = function (eventName, data) {
        var event = this.events[eventName];
        if (event) {
            event.fire(data);
        }
    };
    Multi_gifuser.prototype.on = function (eventName, callback) {
        var event = this.events[eventName];
        if (!event) {
            event = new DispatcherEvent(eventName);
            this.events[eventName] = event;
        }
        event.registerCallback(callback);
    };
    Multi_gifuser.prototype.off = function (eventName, callback) {
        var event = this.events[eventName];
        if (event) {
            delete this.events[eventName];
        }
    };
    return Multi_gifuser;
}());
/////////////
// function diamond() {
//     var _a;
//     this.onInit = function () {
//         var THREE = this.context.three;
//         giffigure.THREE = THREE;
//         var geometry = new THREE.PlaneGeometry(0.5, 0.5);
//         console.log("diamond icon");
//         console.log(giffigure.chatrecommondcounter);
//         var diamonddetail = giffigure.recommendlist[giffigure.chatrecommondcounter];
//         console.log(diamonddetail);
//         var img = 'VIP-diamonds.gif';
//         if (!diamonddetail.isVIP) {
//             img = 'cube-men.gif';
//             if (diamonddetail.gender.toLowerCase() == 'female') {
//                 img = 'cube-lady.gif';
//             }
//         }
//         var path = giffigure.gifdancerpath + "/jack_img/" + img;
//         console.log(path);
//         // var texture = THREE.ImageUtils.loadTexture(path)
//         var texture = new THREE.TextureLoader().load(path);
//         texture.minFilter = THREE.LinearFilter;
//         var material = new THREE.MeshBasicMaterial({
//             map: texture,
//             color: 0xffffff,
//             side: THREE.DoubleSide,
//             transparent: true
//         });
//         material.map.needsUpdate = true;
//         var tplane = new THREE.Mesh(geometry, material);
//         // console.log("init")
//         // console.log(giffigure.counter)
//         tplane.name = "diamond_" + giffigure.counter;
//         this.outputs.objectRoot = tplane;
//         this.outputs.collider = tplane;
//     };
//     this.events = (_a = {},
//         _a["INTERACTION.CLICK"] = true,
//         _a);
//     this.onEvent = function (type, data) {
//         console.log(type);
//         console.log(data.collider.name);
//         giffigure.gifdanceraction(data);
//     };
// }
function gif_a() {
    this.onInit = function () {
        var x, y, z;
        x = tmpPosX;
        y = tmpPosY;
        z = tmpPosZ;
        var _this = this;
        var THREE = this.context.three;
        giffigure.clock = new THREE.Clock();
        giffigure.THREE = THREE;
        var geometry = new THREE.PlaneGeometry(2.06, 3.5);
        //var material = new THREE.MeshBasicMaterial({color: 0xffffff,side:THREE.DoubleSide} )
        // var random =  Math.floor(Math.random() * 13) + 1;
        var random = 14;
        // giffigure.avatarkeygif[giffigure.counter] =  random
        var path = 'https://cdn.jsdelivr.net/gh/duchieupham/matterport@main/gif_dance-0' + indexFile +'.png';
        if(indexFile >= 7 ){
            path = 'https://cdn.jsdelivr.net/gh/duchieupham/matterport@main/gif_dance-015.png';

        }
        
        console.log(path);
        // var texture = THREE.ImageUtils.loadTexture(path)
        var split = 2;
        new THREE.TextureLoader().load(path, function (tex) {
            var width = tex.image.width / split;
            var height = tex.image.height;
            //console.log(width,height)
            var geoheight = 3.5;
            var geowidth = geoheight * width / height;
            // var geometry = new THREE.PlaneGeometry(2.06, 3.5)
            var geometry = new THREE.PlaneGeometry(geowidth, geoheight);
            var material = new THREE.MeshBasicMaterial({
                map: tex,
                color: 0xffffff,
                side: THREE.DoubleSide,
                transparent: true
            });
            giffigure.actionSprite = giffigure.spriteMixer.ActionSprite(tex, 2, 1);
            // console.log( giffigure.actionSprite)
            giffigure.actionSprite.setFrame(0);
            giffigure.actions.anim = giffigure.spriteMixer.Action(giffigure.actionSprite, 0, 2, 200);
            giffigure.actions.anim.playLoop();
            giffigure.loop();
            material.map.needsUpdate = true;
            var tplane = new THREE.Mesh(geometry, material);
            // console.log("init")
            // console.log(giffigure.counter)
            tplane.name = "gif_" + giffigure.counter;
            _this.outputs.objectRoot = tplane;
            _this.outputs.collider = tplane;
        });

        this.events = (_a = {},
            _a["INTERACTION.CLICK"] = true,
            _a["INTERACTION.HOVER"] = true,
            _a);
        this.onEvent = function (type, data) {
            if (type == "INTERACTION.CLICK") {
                // walk.isStop = true;
                // walk.openChatIframe();
  
                // window.postMessage('Clicked DJ', '*');
                console.log('CLICKED IMAGE');
                embedImageClicked(x, y + 0.6, z);
            }
        };
    };
}