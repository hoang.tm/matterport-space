// "use strict";

var _sid;

var _mpSdk;

var _stype;

var _scene;

var _config;

var _lastposit, _lastsweep, _mattertags,_mpid , _murl,_murlparameter;
var _lastmode;

var ct = new Commontools()

var _userbalance;
var _config_param;
var _isEditMode = false;
var _isProduction = false;

var _isDevelopmentMode = false;

var _isIPhone = false;
var _isMobile = false;
var _isJqueryInit = false;
var _userbalance,_userId;
var _isPaid, _ispayment, _isTagList, _scenetype, _scenetype_list, _sorting = '';

var _isStartBtn,_isShareBtn,_smartlink;

var _lastRotation;

var player,_bgmId = '';

var _pose , _zoom;


var _nodejsport;


var _isSubModule = false,_SubModule;

var _allSweepArray = [];
var _is_full_btn = 1,_is_bundle_show_tag;
var _isSDKLoaded = false;
var LOG_PREFIX = new Date().getDate() + '.' + new Date().getMonth() + '.' + new Date().getFullYear() + ' / ' + new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
var log = console.log;
var _isSubBundle = false, _isBundle = 0;
var _mastertype;
var _ismenu_onoff = 0;
var elemenu_setting;
var _isbundlelight = 1;
var _isvr;
var _vn_asset_path = `${window.location.origin}/vn_assets/`;
var _ispremium;
var _authkey = false;
var _eleSetting = "";
var _isfree = true,_sub_expiry_date = "";
var _button_bg_color,_button_fa_color;
var _isbaseClass = true,_isinvtag = true;
var _isbasefunctionloaded = false;
var _submodule_check = false;
var _isShare = 1,_is360 = 1;
var _tagicon_list = {},_edit_tagicon_type = "",_edit_tagicon_id = "",_tagicon_path_array = {};
function configNodeJSPort(){

/*
dev : 5004
test : 5005
pro : 5006
*/

    _nodejsport = 5004;

    console.log(window.location);

    var host = window.location.host;


    if (host == "test.l2vr.co"){

        _nodejsport = 5005;
    }
    else if (host == "l2vr.co" || host ==  "www.l2vr.co"){
        _nodejsport = 5006
    }


}

function getConfig() {

    configNodeJSPort()

    ct.api({"module":"generic", "action": "getConfig" }).then((result) => {



        if (!result.error) {
            _config = { languages: result.languages, scenetype: result.scenetype };

            _scenetype_list = result.scenetype;
            _config_param = result.config_param;
            console.log("_config_param");
            console.log(_config_param);
            if (ct.getUrlParameter("edit")) {

                if (ct.getUrlParameter("edit") == 1) {

                    _authkey = ct.getCookie("authkey");
                    
                    if (_authkey) {
                        var param = {};
                        param.action = "free_try_checking";
                        param.sid = _sid;
                        param.module = 'scene';
                        ct.api(param).then((result)=>{  
                            _isfree = result.isfree;
                            _sub_expiry_date = result.sub_expiry_date;
                            console.log(result)
                            var param = {};
                            param.action = "auth_authenticate";
                            param.sid = _sid;
                            param.module = 'user';
                            ct.api(param).then((result)=>{   

                                if (result.error) {
                                    window.location.replace("login");
                                }
                                else {

                                    _isEditMode = true;
                                    if(_isEditMode){
                                        var hide_btn = 0
                                        if(typeof ct.getUrlParameter('hide') != 'undefined'){
                                            hide_btn = ct.getUrlParameter('hide');
                                        }
                                        if(hide_btn == 0){
                                            $("#container").css({    "top": "63px",
                                            "left": "88px",
                                            "width": "calc(100% - 88px)",
                                            "height": "calc(100% - 63px)"});
                                        }
                                    }
                                    _userbalance = result.userbalance;
                                    _userId = result.userid;
                                    _mastertype = result.mastertype;
                                    _ispremium = result.ispremium;
                                    ct.ajax(false,{"action":"getPaid","sid":_sid}).then((result)=>{
                                        console.log(`isPaid`)
                                        _isPaid = result.isPaid;
                                    
                                        if(_ispremium == 1){
                                            _isPaid = 1;
                                        }
                                    
                                        _ispayment = parseInt(result.ispayment);
                                        console.log(Boolean(_ispayment));
                                        if(_isPaid == 0 && _isEditMode && Boolean(_ispayment)){
                                    
                                        }
                                        handleScene()
                                    
                                    })
                                    
                                }

                                console.log(`isEditable ${_isEditMode}`)
                                console.log(`isProduction ${_isProduction}`)
                            })
                        });
                    }
                    else {

                        window.location.replace("login");
                    }
                }
            }
            else {
                var param = {};
                param.action = "free_try_checking";
                param.sid = _sid;
                param.module = 'scene';
                ct.api(param).then((result)=>{  
                    _ispremium = result.ispremium;
                    _isfree = result.isfree;
                    _sub_expiry_date = result.sub_expiry_date;
                    handleScene()
                });
               
                
            }

            
            
        }

    })

}

function get_tagicon(){
    _tagicon_list = {};
    return new Promise((resolve, reject) => {
        var dt = Date.now();
        var path = _vn_asset_path+'tagicon/json/'+_sid+'.txt?t='+dt;
        checkURL(path, function(status){
            if(status == 200 || status == 0){
                $.get(path,(data)=>{
                    
                    data = ct.htmlspecialdecode(data);
                    console.log(data);
                    _tagicon_list = jQuery.parseJSON(data);
                    build_tagicon();
                   
                }).always(function() {
                    resolve(true);
                });
            }else{
                resolve(true);
              }
        });
    });

}

function build_tagicon(){
    $.each(_tagicon_list,function(k,v){
        _mpSdk.Mattertag.resetIcon(k);
        if(v.hasOwnProperty("path")){
            var path = _vn_asset_path + v.path.split("vn_assets/")[1];
            var filename = v.path.split("/")[4];
            _tagicon_path_array[filename] = path;
        }
    });
    $.each(_tagicon_path_array,function(k,v){
        _mpSdk.Mattertag.registerIcon(k, v);
    })
    $.each(_tagicon_list,function(k,v){
        if(v.hasOwnProperty("path")){
            var filename = v.path.split("/")[4];
            _mpSdk.Mattertag.editIcon(k, filename);
        }
    });
    console.log(_tagicon_path_array);
}

function save_tagicon(){
    var tagicon_str = JSON.stringify(_tagicon_list);
      
    var param = { 
        'module': "scene",
        'action': "auth_save_taglist_json",
        'sid': _sid,
        'tagicon_json': tagicon_str,
    };
    
    ct.api(param).then((obj)=>{
        console.log(_tagicon_list);
        build_tagicon();
    });

}

function del_tagicon(file_url){
      
    var param = { 
        'module': "scene",
        'action': "auth_del_tagicon",
        'sid': _sid,
        'file_url': file_url,
    };
    
    ct.api(param).then((obj)=>{
        if (!obj.error) {
            console.log(obj.desc);
        }else{
            console.log(obj);
        }
    });

}

function checkURL(url,cb){
    try {
        ct.api({ action: 'UrlExists',module:'generic' ,url: url}).then((obj) => {
          
          if (!obj.error) {
            cb(obj.data);
          }
        });
    }catch (err) {
        console.log(err);
    }
}


function upload_tagicon(file = null){
    s_getBase64(file).then((data)=> {
        if(data == null){
            if(_edit_tagicon_type == "removetag"){
                var edit_url = _tagicon_list[_edit_tagicon_id].path;
                var tagtype = _tagicon_list[_edit_tagicon_id].type;
                $.each(_mattertags,function(k,v){
                    if(v.sid == _edit_tagicon_id){
                        if(_tagicon_list.hasOwnProperty(v.sid)){
                            
                            _tagicon_list[v.sid] = {};
                        }
                    }
                });
                if(tagtype !== "alltag"){
                    del_tagicon(edit_url);
                }
            }else if(_edit_tagicon_type == "removealltag"){
                var edit_url = _tagicon_list[_edit_tagicon_id].path;
                $.each(_mattertags,function(k,v){
                    if(_tagicon_list.hasOwnProperty(v.sid)){
                        if(_tagicon_list[v.sid].type == "alltag"){
                            _tagicon_list[v.sid] = {};
                        }
                    }
                });
                del_tagicon(edit_url);
            }else if(_edit_tagicon_type == "alltag"){
                var edit_url = _tagicon_list[_edit_tagicon_id].path;
                var filename = _tagicon_list[_edit_tagicon_id].filename;
                $.each(_mattertags,function(k,v){
                    if(_tagicon_list.hasOwnProperty(v.sid)){
                        if(_tagicon_list[v.sid].type == "alltag"){
                            _tagicon_list[v.sid].path = edit_url;
                            _tagicon_list[v.sid].filename = filename;
                        }
                    }
                });
            }else if(_edit_tagicon_type == "thistag"){
                _tagicon_list[_edit_tagicon_id].type = "thistag";
            }
            console.log(_tagicon_list);
            save_tagicon();
            if($(".tagicon_div").length > 0){
                $(".tagicon_div").remove();
                $("#taglist").click();
            }
        }else{
            file64 = data;

            var param = { 
                'module': "scene",
                'action': "auth_save_tagicon",
                'sid': _sid,
                'userId': _userId,
                'file64': file64,
                'file_name': file.name,
                'tagid': _edit_tagicon_id,
            };
            
            ct.api(param).then((obj)=>{
                console.log(obj);
                if(!obj.error){
                    console.log(_edit_tagicon_type)
                    console.log(_edit_tagicon_id)
                    if(_edit_tagicon_type == "alltag"){
                        $.each(_mattertags,function(k,v){
                            if(!_tagicon_list.hasOwnProperty(v.sid)){
                                _tagicon_list[v.sid] = {};
                            }else{
                                if(_tagicon_list[v.sid].hasOwnProperty("path")){
                                    var edit_url = _tagicon_list[v.sid].path;
                                    del_tagicon(edit_url);
                                }
                            }
                            _tagicon_list[v.sid].path = obj.fileroot_path;
                            _tagicon_list[v.sid].type = _edit_tagicon_type;
                            _tagicon_list[v.sid].filename = file.name;
                        });
                    }else if(_edit_tagicon_type == "thistag"){
                        $.each(_mattertags,function(k,v){
                            if(v.sid == _edit_tagicon_id){
                                if(!_tagicon_list.hasOwnProperty(v.sid)){
                                    _tagicon_list[v.sid] = {};
                                }else{
                                    if(_tagicon_list[v.sid].hasOwnProperty("path")){
                                        var edit_url = _tagicon_list[v.sid].path;
                                        if(_tagicon_list[v.sid].type !== "alltag"){
                                            del_tagicon(edit_url);
                                        }
                                    }
                                }
                                _tagicon_list[v.sid].path = obj.fileroot_path;
                                _tagicon_list[v.sid].type = _edit_tagicon_type;
                                _tagicon_list[v.sid].filename = file.name;
                            }
                            
                        });
                    }
                    save_tagicon();
                    if($(".tagicon_div").length > 0){
                        $(".tagicon_div").remove();
                        $("#taglist").click();
                    }
                };
            });
        }
        
    });
}

function modify_icon(){

}

function taglist(mattertags) {
    console.log(mattertags)
    if(mattertags[0]['mediaType'].includes("mattertag.media")){
        var sort_video = _config_param.tag_type_video;
        var sort_photo = _config_param.tag_type_photo;
        var sort_rich = _config_param.tag_type_rich;
        var sort_none = "mattertag.media.none";
    }else{
        var sort_video = _config_param.bundle_tag_type_video;
        var sort_photo = _config_param.bundle_tag_type_photo;
        var sort_rich = _config_param.bundle_tag_type_rich;
        var sort_none = "none";
    }

    text = `<p class="title">Mattertag List</p><hr>`
   
    text += `
            <form id="app-cover" style="top:68px;width:88%;">
                    <div id="select-box" class="tag_type">
                            <input type="checkbox" id="options-view-button" class='scene_input'>
                            <div id="select-button" class="brd">
                                    <div id="selected-value">
                                    <span class='data_modified'>Distance</span>
                                    </div>
                                    <div id="chevrons">
                                    <img src='./assets/img/btn/icn_dropdown.svg' width='16px'></img>
                                    </div>
                            </div>
                            <div id="options"  style="width:88%;">
                            <div class="option">
                                        <input class="s-c top" type="radio" name="platform" data-sort="distance" value="Distance">
                                        <input class="s-c bottom" type="radio" name="platform" value="Distance">
                                        
                                        <span class="label">Distance</span>
                                        
                                </div>
                                <div class="option">
                                        <input class="s-c top" type="radio" name="platform" data-sort="${sort_video}" value="Video">
                                        <input class="s-c bottom" type="radio" name="platform" value="Video">
                                        
                                        <span class="label">Video</span>
                                        
                                </div>
                                <div class="option">
                                        <input class="s-c top" type="radio" name="platform" data-sort="${sort_photo}" value="Image">
                                        <input class="s-c bottom" type="radio" name="platform" value="Image">
                                        
                                        <span class="label">Image</span>
                                        
                                </div>
                                <div class="option">
                                        <input class="s-c top" type="radio" name="platform" data-sort="${sort_none}" value="Text">
                                        <input class="s-c bottom" type="radio" name="platform" value="Text">
                                        
                                        <span class="label">Text</span>
                                        
                                </div>
                                    <div id="option-bg"></div>
                            </div>
                    </div>
            </form>`;

    text += ` <div class="search-container" style="margin-left:0;padding: 0px;margin-top: 61px;">
                <input type="text" placeholder="Search" name="search" class='tagsearchInputbox' style="margin-left:auto;color: #fff;width: 100%;background: #404040;">
                <div class='tagsearcgbut'><i class="fa fa-search fa_color"></i></div>
            </div>
            <div class='Fill_button' style='margin-top:10px;width: 100%;width: 100%;display: inline-flex;'>
            </div>`

    var taglist = `<div class="row item-row" style="margin-bottom:1rem;margin-left:0px;max-height: calc(100vh - 264px);overflow: scroll;">`;
    mattertags.forEach(element => {
        //console.log(element['parsedDescription'].length);

        console.log(element)
        if (element['enabled']) {


            var description = element['description'];

           // console.log(description)
            var reg =  /\[.*?\]\([^ ]{1,}\)/g

            var match = description.match(reg);

            if (match){

                console.log(match)

                match.forEach(element=>{
                    var strs = element.split("](");

                    if (strs.length == 2){

                        var text = strs[0].substr(1);
                        description =  description.replace(element,text)
                        console.log(description)
                    }


                })
                
            }


            console.log(description)
            console.log(element['label']);
            if(_tagicon_list.hasOwnProperty(element["sid"]) && _tagicon_list[element["sid"]].hasOwnProperty("path")){
                var tagicon = true;
                var tagicon_path = _vn_asset_path + _tagicon_list[element["sid"]].path.split("vn_assets/")[1]
            }else{
                var tagicon = false;
            }
            
            taglist += `<div class="col-lg-12 col-sm-12 col-12 taglist_content" style='white-space: nowrap;overflow: hidden;text-overflow: ellipsis;'><a href="javascript:nav_Tag('${element['sid']}');" style='width:100%;'>`;
            if (element['mediaType'] == sort_photo) {
                taglist += `<span style="color: #404040;"><i class="fa fa-picture-o" aria-hidden="true" style="margin-right: 5px;"></i>`;
                if(_isEditMode){
                    taglist += `<i class="fa fa-pencil tagimg_edit" aria-hidden="true" data-index="${element["sid"]}" data-label="${element['label']}"></i>`;
                    if(tagicon){
                        taglist += `<img src="${tagicon_path}" style="    width: 16px;
                        height: 16px;
                        margin-left: 5px;">`
                    }
                    
                }
                taglist += `<span> ${element['label']}</span><br><span style="font-size: 0.8rem;">${description}</span>`


                taglist += `</span>`;


            }

            else if (element['mediaType'] == sort_video) {
                taglist += `<span style="color: #404040;"><i class="fa fa-play-circle-o" aria-hidden="true" style="margin-right: 5px;"></i> `
                if(_isEditMode){
                    taglist += `<i class="fa fa-pencil tagimg_edit" aria-hidden="true" data-index="${element["sid"]}" data-label="${element['label']}"></i>`;
                    if(tagicon){
                        taglist += `<img src="${tagicon_path}" style="    width: 16px;
                        height: 16px;
                        margin-left: 5px;">`
                    }
                }
                taglist += `<span> ${element['label']}</span><br><span style="font-size: 0.8rem;">${description}</span>`



                taglist += `</span>`;
            
            }else if(element['mediaType'] == sort_rich ){
                taglist += `<span style="color: #404040;"><i class="fas fa-photo-video" style="margin-right: 5px;"></i></i> `
                if(_isEditMode){
                    taglist += `<i class="fa fa-pencil tagimg_edit" aria-hidden="true" data-index="${element["sid"]}" data-label="${element['label']}"></i>`;
                    if(tagicon){
                        taglist += `<img src="${tagicon_path}" style="    width: 16px;
                        height: 16px;
                        margin-left: 5px;">`
                    }
                }
                taglist += `<span> ${element['label']}</span><br><span style="font-size: 0.8rem;">${description}</span>`



                taglist += `</span>`;
            }
            else{
                taglist += `<span style="color: #404040;"><i class="fa fa-font" aria-hidden="true" style="margin-right: 5px;"></i>`
                if(_isEditMode){
                    taglist += `<i class="fa fa-pencil tagimg_edit" aria-hidden="true" data-index="${element["sid"]}" data-label="${element['label']}"></i>`;
                    if(tagicon){
                        taglist += `<img src="${tagicon_path}" style="    width: 16px;
                        height: 16px;
                        margin-left: 5px;">`
                    }
                }
                taglist += ` <span> ${element['label']}</span><br><span style="font-size: 0.8rem;">${description}</span>`
                taglist += `</span>`;
            }
            taglist += `</a></div>`;
        }

    });

    taglist += `</div>`;
    text += taglist;


    if (!$('.tag_type').length) {
        /*var tagList = bootbox.dialog({
            className: 'tagList',
            title: 'Mattertag List',
            message: text,
            size: 'medium',
            backdrop: false,
            onEscape: false,
        });*/
        $('.side_menu_div').html(text);

        $('.tagsearcgbut').click(function(){
            search_Tag();
        });

        $(".tagimg_edit").click(function(){
            $("#taglist").click();
            if($(".tagicon_div").length > 0){
                $(".tagicon_div").remove();
                $("#taglist").click();
            }
            console.log($(this).data("index"));
            _edit_tagicon_id = $(this).data("index");
            var dialog_title = `<p>${$(this).data("label")}</p><i class="fa fa-times tagicon_close" aria-hidden="true"></i>`;
            var dialog_body = `<div class="tagicon_body">
                <span style="padding-left:20px;">You can upload .png, .jpg and .jpeg icon file.</span>
                <br><span style="padding-left:20px;">Max. dimension is: 125 x 125 px.</span>
                </br>
                <div class="upload_div">
                   
                    <span class="tag_icon_name"></span>
                    <button class="add_tagicon"><span class="span_a" style="font-size: 25px;"> + </span><span class="span_b" style="font-size: 12px;">New Tag icon</span></button>
                </div>
                <input type="file" class="media_file" id="media_file" style="display:none"/>
                <br>
                <div class="tagtype">
                    <span>Apply to : </span>
                    <input type="radio" id="alltag" name="tagicon_type" value="alltag">
                    <label for="alltag">All Tags</label>
                    <input type="radio" id="thistag" name="tagicon_type" value="thistag" style="margin-left: 92px;">
                    <label for="thistag">This Tag</label><br>
                 
                    <input type="radio" id="removealltag" name="tagicon_type" value="removealltag" style="margin-left: 81px;display:none;">
                    <label for="thistag" style="display:none;">Reset All Icons</label>
                    <input type="radio" id="removetag" name="tagicon_type" value="removetag" style="margin-left: 38px;display:none;">
                    <label for="thistag" style="display:none;">Reset This Icon</label>
                </div>
                <span class="tagicon_error_msg"></span>
                <button class="tagicon_upload_btn">Upload tag icon</button>
            </div>`;
            var dialog = `<div class="tagicon_div">`;
            dialog += dialog_title;
            dialog += dialog_body;
            dialog += `</div>`;
            

            $("body").append(dialog);
            if(Object.keys(_tagicon_list).length > 0){
                if(_tagicon_list.hasOwnProperty(_edit_tagicon_id) && Object.keys(_tagicon_list[_edit_tagicon_id]).length >0){
                    console.log(_tagicon_list[_edit_tagicon_id]);
                    $(".tag_icon_name").html(_tagicon_list[_edit_tagicon_id].filename);
                    $(".tagtype").show();
                    $(".tagicon_upload_btn").show();
                    $(".tagicon_upload_btn").html("Change Tag Icon");
                    $(".tagicon_upload_btn").click(function(){
                        upload_tagicon(null);
                    });
                    
                    $('input[type=radio][name=tagicon_type]').eq(3).show();
                    $(".tagtype label").eq(3).show();
                    
                    if(_tagicon_list[_edit_tagicon_id].type == "alltag"){
                        $('input[type=radio][name=tagicon_type]').eq(0).attr('checked', 'checked');
                        _edit_tagicon_type = $('input[type=radio][name=tagicon_type]').eq(0).val();
                        $('input[type=radio][name=tagicon_type]').eq(2).show();
                        $(".tagtype label").eq(2).show();
                    }else{
                        $('input[type=radio][name=tagicon_type]').eq(3).css({"margin-left":"82px"});
                        $('input[type=radio][name=tagicon_type]').eq(1).attr('checked', 'checked');
                        _edit_tagicon_type = $('input[type=radio][name=tagicon_type]').eq(1).val();
                    }
                }else{
                    $('input[type=radio][name=tagicon_type]').eq(1).attr('checked', 'checked');
                    _edit_tagicon_type = $('input[type=radio][name=tagicon_type]').eq(1).val();
                }
            }else{
                $('input[type=radio][name=tagicon_type]').eq(1).attr('checked', 'checked');
                _edit_tagicon_type = $('input[type=radio][name=tagicon_type]').eq(1).val();
            }

            $('input[type=radio][name=tagicon_type]').change(function() {
               _edit_tagicon_type = $(this).val();
               console.log(_edit_tagicon_type);
            })

            $('.add_tagicon').click(function(){
                $(":file").val('');
                $('.media_file').click();
            });

            $(".tagicon_close").click(function(){
                $(".tagicon_div").remove();
                $("#taglist").click();
            });
            

            $(':file').on('change', function () {
                var file = this.files[0];
                console.log(file);
                var filecheck = false;
                if (file.type == "image/pjpeg" || file.type == "image/jpeg" || file.type == "image/png") {
                    $(".tagicon_error_msg").html("");
                    $(".tag_icon_name").html(file.name);
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function (e) {
                        var image = new Image();
                        image.src = e.target.result;
                        image.onload = function () {
                            var height = this.height;
                            var width = this.width;
                            console.log(height + " " + width);
                            if (height > 125 || width > 125) {
                                $(".tagicon_error_msg").html("Error: The icon exceeds dimension limit (125 x 125 px).");
                                $(".tagtype").hide();
                                $(".tagicon_upload_btn").hide();
                            }else{
                                filecheck = true;
                                $(".tagtype").show();
                                $(".tagicon_upload_btn").show();
                                $(".tagicon_upload_btn").unbind("click");
                                $(".tagicon_upload_btn").click(function(){
                                    upload_tagicon(file);
                                });
                            }
                        };
                    };
                }else{
                    $(".tag_icon_name").html(file.name);
                    $(".tagicon_error_msg").html("Please upload image");
                    $(".tagtype").hide();
                    $(".tagicon_upload_btn").hide();
                }
            });
        });

        $('.tagsearchInputbox').keyup(function (e) {
            search_Tag();
        });

        $('.option').click(function(){
            console.log($('input[name="platform"]:checked').val());
            console.log($(this).find('.top').attr('data-sort'));
            $('.data_modified').html($('input[name="platform"]:checked').val());
            
            sort_tag($(this).find('.top').attr('data-sort'));

            
        });
    
        $('.option').hover(
            function(){
                $(this).css({ "background-color": "rgba(241, 241, 241, 1)"});
            }, function() {
                $('.option').css({ "background-color": "#fff"});
            }
        )
    
        $('#options').hover(function(){},function(){
            $(this).parent().find('#options-view-button').click();
            $('.option input[type="radio"]').prop('checked',false);
            console.log('out')
        })
    
        /*tagList.css({ 'pointer-events': 'none', 'z-index': '3050' });
        tagList.find('.modal-content').css({ 'background-color': 'rgba(255, 255, 255, 0.85)' });
        $('.modal.tagList').css({ 'width': ' 0 !important', 'height': '0 !important' });

        $('.modal.tagList .modal-dialog').css({ 'position': 'fixed', 'top': '0px', 'right': '2%', 'width': '90%','z-index': '3050' });
        $('.modal.tagList .modal-dialog .modal-header .modal-title').css({ 'color': '#404040' });
        $('.modal.tagList .modal-dialog .modal-body').css({ 'padding': '0rem 1rem' });*/

        if (_sorting != '') {
            sort_tag(_sorting);
        }
    } else {
        $('.item-row').remove();
        $('.side_menu_div').append(taglist);
    }

}

function menu(){
    menu_list = `<div class="is_menu_div">`;

    menu_list += `
    <div id='menu_onoff_switch'>
                <label for="menu_onoff">Menu Setting</label>
                <div class="menu_onoff_input form-check-inline">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="menu_onoff" value='${_ismenu_onoff}'>
                        <label class="custom-control-label" for="menu_onoff"></label>
                    </div>
                </div>
            </div>
    <hr>`;

    
    menu_list += `</div>`;

    $('.side_menu_div').append(menu_list);

    if($('#menu_onoff').val() == 0){
        $('#menu_onoff').prop('checked', false);
        _ismenu_onoff = 0;
    }else{
        $('#menu_onoff').prop('checked', true);
    
        _ismenu_onoff = 1;
    }
}


function search_Tag() {
    var mattertags = _mattertags;
    var search_array = [];
    if ($('.tagsearchInputbox').val() != '' && $('.tagsearchInputbox').length) {
        var search_result = mattertags.filter(function (item, index, array) {
            if (item.label.toLowerCase().includes($('.tagsearchInputbox').val().toLowerCase()) || item.description.toLowerCase().includes($('.tagsearchInputbox').val().toLowerCase())) {
                search_array.push(item);
            }
        });
    } else {
        search_array = _mattertags;
    }
    if (search_array.length == 0) {
        $('#sort').css({ 'display': 'none' });
    } else {
        $('#sort').css({ 'display': 'block' });
    }
    taglist(search_array);
}

function sort_tag(sort) {
    console.log(sort)
    _sorting = sort;
    var mattertags = _mattertags;
    if(mattertags[0]['mediaType'].includes("mattertag.media")){
        var sort_video = _config_param.tag_type_video;
        var sort_photo = _config_param.tag_type_photo;
        var sort_rich = _config_param.tag_type_rich;
    }else{
        var sort_video = _config_param.bundle_tag_type_video;
        var sort_photo = _config_param.bundle_tag_type_photo;
        var sort_rich = _config_param.bundle_tag_type_rich;
    }
    $('.sort').css({ 'background': '#007bff', 'color': '#fff' });
    $("#" + sort).css({ 'background': 'rgba(255, 255, 255, 0.85)', 'color': '#404040' })

    var mattertags = _mattertags;
    
    if (sort == 'distance') {
        $.each(mattertags, (index, tag) => {
            mattertags[index]['posit'] = Math.sqrt((Math.pow(tag.anchorPosition['x'] - _lastposit['x'], 2) + Math.pow(tag.anchorPosition['y'] - _lastposit['y'], 2) + Math.pow(tag.anchorPosition['z'] - _lastposit['z'], 2)));
        })
        sortByKeyAsc(mattertags, 'posit');
    }else {

        console.log(sort)
        console.log(sort_photo)
        console.log('mattertags67')
        console.log(mattertags);
        mattertags = jQuery.grep(mattertags, function (a) {
            
            if (sort == sort_photo){
                return a['mediaType'] == sort_rich || a['mediaType'] == sort_photo;
            }
            else if(sort == sort_video){
                return a['mediaType'] == sort_rich || a['mediaType'] == sort_video;
            }
            else{
                return a['mediaType'] == sort;
            }
            
        });
        sortByKeyAsc(mattertags, 'label');
    }
    if(mattertags.length > 0){
        taglist(mattertags);
    }else{
        $('.side_menu_div').find('.item-row').html('');
    }
    
}

function nav_Tag(tagid) {
    
    if(_scenetype == 'invisible_mattertag'){
        var exclude;
        
        jQuery.grep(_invisible_mattertags, function( a ) {
            if(a['sid'] == tagid){
                exclude = a['exclude'];
            }
        });

        if(_isbillboard){
            if(exclude == 1){
                _mpSdk.Mattertag.preventAction(tagid, {
                    opening: false,
                });
            }else{
                _mpSdk.Mattertag.preventAction(tagid, {
                    opening: true,
                });
            }
        }else{
            _mpSdk.Mattertag.preventAction(tagid, {
                opening: false,
            });
        }
    }
    
    _mpSdk.Mattertag.navigateToTag(tagid, _mpSdk.Mattertag.Transition.FLY)
    .then(function (e) {
        if(_scenetype == 'invisible_mattertag'){   
            if(_isbillboard){
                tagclick(tagid); 
            }
        }
       
    });
    $('.modal.tagList').remove();
}

function sortByKeyDesc(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
}

function sortByKeyAsc(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}



function savelog(e){

    console.debug(e)
}

$(function () {
    $(document).on ('mozfullscreenchange webkitfullscreenchange fullscreenchange',function(){
        var fse = document.fullscreenElement||document.mozFullScreenElement||document.webkitFullscreenElement||document.msFullscreenElement;

        if(fse){//if there is a fullscreened element
            $('.full_btn .icon').html('<i class="fas fa-compress"></i>')
        }else{//if nothing is in full screen
            $('.full_btn .icon').html('<i class="fas fa-expand"></i>')
        }
     
      
    });

   
    
    
    
    if (!_isJqueryInit) {

        _isJqueryInit = true;

        configNodeJSPort();

        console.log("scene init")

        console.log(window.location);

        if (window.location.hostname != "dev.l2vr.co" && window.location.hostname != "test.l2vr.co"  && window.location.hostname != "new.l2vr.co" && window.location.hostname != "newtest.l2vr.co"  && window.location.hostname != "yin.l2vr.co") {
            _isProduction = true;

                console.log = function () { };

                console.log(`isProduction ${_isProduction}`)
            
        }


        if (ct.getUrlParameter("log") && window.location.hostname == "dev.l2vr.co"){

            ct.getUrlParameter("log") == "1";

            console.log("log console");

            console.log = function (e){
                //var args = Array.from(arguments);

                var args = Array.prototype.slice.call(arguments, 0)

                log.apply(console,args)

                var param = {};
                param.module = "generic"
                param.action = 'debuglog'
                param.data = JSON.stringify(args).substring(0,100)

                ct.api(param)
            }

        }

        if (window.location.hostname == "dev.l2vr.co" || window.location.hostname == "remote.l2vr.co" || window.location.port == "5008") {

            _isDevelopmentMode = true;
        }

        get_scene_info().then((result)=> {
            _sid = ct.getUrlParameter("sid")
        
            _isEditMode = ct.getUrlParameter("edit")
        
            
            if(_isEditMode == 1){
                _isEditMode = true;
            }else{
                _isEditMode = false;
                $("#buttonlist").css({"pointer-events": "none"});
            }

            console.log('_isEditMode67')
            console.log(_isEditMode);
    

            console.log(ct.getCookie("visitcookie"))
            if (!ct.getCookie("visitcookie")) {

                console.log(Date.now())
                console.log(ip);

                var visitcookie = Date.now() + md5(ip + Date.now())

                ct.setCookie("visitcookie", visitcookie, 30)

            }

            var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

            console.log(navigator)

            _isIPhone = iOS;


            //***************************** must remove for dev test
            if (window.location.hostname == "dev.l2vr.co") {

            // _isIPhone = true;
            }

            //****************************************

            if (_isIPhone) {
                console.log("this is iphone")
            }
            else {
                console.log("this is not iphone");
            }


            if(Commontools.isMobile()) { 
                _isMobile = true;
            }
            if(window.navigator.userAgent.includes("OculusBrowser")){
                _isMobile = true;
            }


            getConfig()

            bootbox.setDefaults({

                backdrop: true,
                closeButton: true,
                centerVertical: true,
            })

            $('.side_bar_div').css({'z-index':'1','top':'63px',    'left': '0'});
            btnlist = $("#buttonlist")
            scenebtn = $('.scene_btn')
            scenebtn.show();
            //btnlist.append(`<button class="btn tbutton buttonlistitem main_color" style="float:left" id=backBtn data-toggle=tooltip title='Back'><i class="fa fa-arrow-left fa_color" ></i></button >`);
            btnlist.append(`<img src='../assets/img/btn/backBtn.svg' width="25" id=backBtn></img>`);
            if (ct.getUrlParameter("edit")==1){
                $("#backBtn").on("click", function () {

                    window.location = "project";
                })
            }

        

            //btnlist.append(` <button class="btn tbutton buttonlistitem main_color" style="float:right;display:none;" id=taglist data-toggle=tooltip><i class="fa fa-circle-o fa_color" ></i></button >`);
            if(_isEditMode){
                console.log("add #buttonlist67")
                scenebtn.append(`<li id=taglist style="display:none;">
                                <a>
                                    <img src='./assets/img/btn/icn_tag_list.svg' width='26' style="padding-right: 1px;">
                                    <span>Mattertag list</span>
                                </a>
                            </li>`);
                $("#taglist").on("click", function () {
                    console.log(_mattertags);
                    if(_mattertags.length > 0){
                        if($('#taglist a').hasClass('clicked')){
                            $('#taglist a').removeClass('clicked');
                            $('.side_menu_div').hide();
                            $('#taglist a').css({"background-color": "rgba(168, 223, 223, 0)"});
                            $('.side_menu_div').html('');
                            $('.close_side_menu').hide();

                            if(typeof scenetype_control == "function"){
                                scenetype_control();
                            }
                        }else{
                            $('.side_menu_div').html('');
                            $('.scene_btn li a').removeClass('clicked');
                            $('.scene_btn li a').css({"background-color": "rgba(168, 223, 223, 0)"});
                            $('#taglist a').addClass('clicked');
                            $('.side_menu_div').show();
                            $('.close_side_menu').show();
                            $('#taglist a').css({"background-color": "rgba(168, 223, 223, 1)"});

                            taglist(_mattertags);
                            if(typeof scenetype_control == "function"){
                                scenetype_control();
                            }
                        }
                    }
                    
                    
                })
            }
            _authkey = ct.getCookie("authkey");
            
            if(_isEditMode && _authkey){
                if(_stype !== "3dnavigation"){
                    scenebtn.append(`<li id=Setting>
                                        <a>
                                            <img src='./assets/img/btn/icn_brand.svg' width='26' style="padding-right: 1px;">
                                            <span>Brand</span>
                                        </a>
                                    </li>`);
                    scenebtn.append(`<li id=welcome_mess>
                                    <a>
                                        <img src='./assets/img/btn/welcome_mess.svg' width='26' style="padding-right: 1px;">
                                        <span>Welcome\nMessage</span>
                                    </a>
                                </li>`);

                }
               

                scenebtn.append(`<li id=menu style='display:none;'>
                            <a>
                                <img src='./assets/img/btn/icn_menu.svg' width='26' style="padding-right: 1px;">
                                <span>Menu</span>
                            </a>
                        </li>`);


                $('#welcome_mess').on("click", function(){
                    if($('#welcome_mess a').hasClass('clicked')){
                        $('#welcome_mess a').removeClass('clicked');
                        $('.side_menu_div').hide();
                        $('#welcome_mess a').css({"background-color": "rgba(168, 223, 223, 0)"});
                        $('.side_menu_div').html('');
                        $('.close_side_menu').hide();

                        if(typeof scenetype_control == "function"){
                            scenetype_control();
                        }
                    }else{
                        $('.side_menu_div').html('');
                        $('.scene_btn li a').removeClass('clicked');
                        $('.scene_btn li a').css({"background-color": "rgba(168, 223, 223, 0)"});
                        $('#welcome_mess a').addClass('clicked');
                        $('.side_menu_div').show();
                        $('.close_side_menu').show();
                        $('#welcome_mess a').css({"background-color": "rgba(168, 223, 223, 1)"});
                        if(typeof scenetype_control == "function"){
                            scenetype_control();
                        }
                        elesetting.setting(true).then((result)=> {
                            if(result){
                                console.log(result);
                                elesetting.load_setting(true);
                            }  
                        });
                    }
                });
        
                $("#Setting").on("click", function () {
        
                    if($('#Setting a').hasClass('clicked')){
                        $('#Setting a').removeClass('clicked');
                        $('.side_menu_div').hide();
                        $('#Setting a').css({"background-color": "rgba(168, 223, 223, 0)"});
                        $('.side_menu_div').html('');
                        $('.close_side_menu').hide();
                        if(typeof scenetype_control == "function"){
                            scenetype_control();
                        }
                    }else{
                        $('.side_menu_div').html('');
                        $('.scene_btn li a').removeClass('clicked');
                        $('.scene_btn li a').css({"background-color": "rgba(168, 223, 223, 0)"});
                        $('#Setting a').addClass('clicked');
                        $('.side_menu_div').show();
                        $('.close_side_menu').show();
                        $('#Setting a').css({"background-color": "rgba(168, 223, 223, 1)"});

                        if(typeof scenetype_control == "function"){
                            scenetype_control();
                        }
                    }
                    console.log(_eleSetting);
                    elesetting.setting(false).then((result)=> {
                        if(result){
                            console.log(result);
                            elesetting.load_setting(false);
                        }  
                    });
        
                })

            }
            
            if(!_isEditMode){
                
                console.log('build taglist btn');
                btnlist = $("#buttonlist")
                btnlist.append(` <button class="btn tbutton buttonlistitem main_color" style="float:right;display:none;" id=taglist data-toggle=tooltip><i class="fa fa-circle-o fa_color" ></i></button >`);
        
                $("#taglist").on("click", function () {
                    if(_mattertags.length > 0){
                        if($('.side_menu_div').is(":visible")){
                            $('.side_menu_div').hide();
                            $('.side_menu_div').html('');
                            $('.close_side_menu').hide();
                        
                        }else{
                            $('.side_menu_div').html('');
                            $('.side_menu_div').show();
                            $('.close_side_menu').show();
                            taglist(_mattertags);
                        
                        }
                    }
                })
            }else{
                $('.side_bar_div').show();
            }
            if(typeof ct.getUrlParameter('hide') != 'undefined'){
                $('#buttonlist').css({    'background-image': 'linear-gradient(rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0))',
                    'background-color': 'rgba(0,0,0,0)'});
                $('.side_bar_div').hide();
            }
            

            

            /*$("#menu").on("click", function () {

                if($('#menu a').hasClass('clicked')){
                    $('#menu a').removeClass('clicked');
                    $('.side_menu_div').hide();
                    $('#menu a').css({"background-color": "rgba(168, 223, 223, 0)"});
                    $('.side_menu_div').html('');
                }else{
                    $('.side_menu_div').html('');
                    $('.scene_btn li a').removeClass('clicked');
                    $('.scene_btn li a').css({"background-color": "rgba(168, 223, 223, 0)"});
                    $('#menu a').addClass('clicked');
                    $('.side_menu_div').show();
                    $('#menu a').css({"background-color": "rgba(168, 223, 223, 1)"});
                    menu();
                }

            })*/

            //btnlist.append(` <button class="btn tbutton buttonlistitem main_color" id=_publishBtn data-toggle=tooltip title='Publish' style="display:none;"><i class="fa fa-upload fa_color" ></i></button >`);
            if(_isEditMode){
                btnlist.append(`<a id=_publishBtn class='font-weight-light top_a'>
                <img src='./assets/img/btn/publish.svg' width='25'>
                <span>Publish</span>
            </a>`);
                $('#_publishBtn').hover(
                    function(){
                        $(this).find("img").attr("src","./assets/img/btn/publish_hover.svg");
                    },function(){
                        $(this).find("img").attr("src","./assets/img/btn/publish.svg");
                    }
                );

                /*btnlist.append(`<a id=payment class='font-weight-light top_a'>
                                    <i class="fa fa-cc-paypal" style="font-size:22px;color:rgba(83, 83, 83, 1);"></i>
                                    <span>PayPal</span>
                                </a>`);

                $("#payment").on("click", function () {
                    payment();
                })*/
            }
        
            $('#_publishBtn').hide();
            //btnlist.append(` <button class="btn tbutton buttonlistitem main_color" id=_shareBtn data-toggle=tooltip title='Share' style="display:none;"><i class="fa fa-share fa_color"></i></button>`);
            if(_stype !== 'voicenavigate'){
                $("#_shareBtn").on("click", function () {
                    console.log(_ispremium)
                    console.log(_isPaid)
                    console.log(_config_param.scene_expiry_alert)
                    if(!_isEditMode && _ispremium == 0 && _isPaid !== 1){
                        iframe.remove();
                        bootbox.confirm({
                            message: `${_config_param.scene_expiry_alert}`,
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function (result) {
                                window.location.replace("main");
                            }
                        });
                    
                    }else{
                        sceneshare();
                    }
                    
                })
            }
            /*if(!_isEditMode){
                btnlist.append(` <button class="btn tbutton buttonlistitem main_color" style="float:right;display:none;" id=startBtn data-toggle=tooltip><i class="fa fa-flag fa_color" ></i></button >`);
            }else{
                btnlist.append(`<a id=startBtn class='font-weight-light top_a'>
                    <i class="fa fa-flag" style="font-size:22px;"></i>
                    <span>Location</span>
                </a>`);
            }

            $("#startBtn").on("click", function () {
                startshare();
            });*/



            if (!_isEditMode){


                window.addEventListener('unload', function() {
                    console.log("unload");

                    
                
                    gasubmitevent("Exit Page");

                    //Pageview('exit');

                    ct.setCookie("reloadbyuser",true,1)
                
                });          
            
        

            }


        
        
        });
    }

    $('.close_side_menu').click(function(){
        /*$('.side_menu_div').hide();
        $('.clicked').css({"background-color": "rgba(168, 223, 223, 0)"});
        $('.side_menu_div').html('');
        $('.close_side_menu').hide();
        $('.clicked').removeClass('clicked');
*/
        $( ".side_menu_div" ).animate({
            opacity: 1,
            left: "+=100",
            height: "toggle"
        }, 300, function() {
            $('.clicked').css({"background-color": "rgba(168, 223, 223, 0)"});
            $('.side_menu_div').html('');
            $('.close_side_menu').hide();
            $('.clicked').removeClass('clicked');

            if(typeof scenetype_control == "function"){
                scenetype_control();
            }
            $( ".side_menu_div" ).css({'left':''});
        });
        $( ".close_side_menu" ).animate({
            opacity: 1,
            left: "+=101",
            height: "toggle"
        }, 300, function() {
            $( ".close_side_menu" ).css({'left':''});
        });

        if($('.photo_show_div').length > 0){
            $.post("api_pusher.php",{"action":"event","actiontype":"end_photo","data":{"success":true},'shareid':_gid},function(result){
                $('.photo_show_div').remove();
            });
        }

});
})


function build_minimap_btn(){

    if (!_isEditMode){

        console.log("show minimap button ");
        console.log(_scene.isShowMinimap,_scene.isMinimapExist)

        if (_scene.isShowMinimap == 1 && _scene.isMinimapExist){

            console.log("show minimap button ");

            var map = new MiniMap_auto()

            //map.show()


            btnlist.append(` <button class="btn tbutton buttonlistitem main_color" id=_minimapbtn data-toggle=tooltip title='Minimap' style="display:block;"><i class="fa fa-map fa_color"></i></button>`);


            $("#_minimapbtn").click(e=>{

                console.log(e)

                map.show()

            })



        }





    }




}




function bulid_share_btn(){
    if(!_isEditMode){
        console.log("_isShare")
        console.log(_isShare);
        if(_isShare == 1){
            btnlist.append(` <button class="btn tbutton buttonlistitem main_color" id=_shareBtn data-toggle=tooltip title='Share' style="display:none;"><i class="fa fa-share fa_color"></i></button>`);
        }
    }else{
        if(typeof ct.getUrlParameter('hide') != 'undefined'){
            if(ct.getUrlParameter('hide') !== "1"){
                btnlist.append(`<a id=_shareBtn class='font-weight-light top_a'>
                <img src='./assets/img/btn/startbtn.svg' width='26'>
                <span>Share</span>
            </a>`);
            }else{
                console.log('show share 67')
                btnlist.append(` <button class="btn tbutton buttonlistitem main_color" id=_shareBtn data-toggle=tooltip title='Share' style="display:none;"><i class="fa fa-share fa_color"></i></button>`);
            }
            
        }else{
            
                btnlist.append(`<a id=_shareBtn class='font-weight-light top_a'>
                    <img src='./assets/img/btn/startbtn.svg' width='26'>
                    <span>Share</span>
                </a>`);
            
            
        }
        
    }

    

    $('#_shareBtn').hover(
        function(){
            $(this).find("img").attr("src","./assets/img/btn/startbtn_hover.svg");
        },function(){
            $(this).find("img").attr("src","./assets/img/btn/startbtn.svg");
        }
    );

    $("#_shareBtn").on("click", function () {
        if(!_isEditMode && _ispremium == 0 && _isPaid !== 1){
            iframe.remove();
            bootbox.confirm({
                message: `${_config_param.scene_expiry_alert}`,
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    window.location.replace("main");
                }
            });
        
        }else{
            startshare();
        }
    })
}

function startshare() {

    console.log("start share");

    console.log(_lastmode)
    console.log('_backgroundcolor67');
    if (_lastmode == "mode.dollhouse" || _lastmode == "mode.floorplan"){

        bootbox.alert("Start Position share does not work for dollhouse / floorplan mode");

        return;
    }

    var param = {};

    param.action = "startshare";
    param.module = "scene";
    param.position = _lastposit;
    param.rotation = _lastRotation;
    param.sweep = _lastsweep;
    param.sid = _sid;


    console.log(param)

    ct.api(param)
        .then((res) => {

            console.log(res);

            if (!res.error) {
                $("#modal_share").modal("show")
                
                _sharelink = `${window.origin}/r/${_sid}?sp=${res.startid}`
                if(typeof _smartlink !== "undefined" && _smartlink !== null ){
                  
                    if( _smartlink.length >= 10)
                    _sharelink = `${window.origin}/r/${_smartlink}?sp=${res.startid}`
                }
                $("#sharelink").val(_sharelink);

               
                
                console.log(_backgroundcolor);
                if(_backgroundcolor !== "undefined"){
                    $('.show .modal-dialog .modal-content').css({'background': _backgroundcolor,'border':0});
                }
            }
            else {

                bootbox.alert("Fail to generate link. Please try later");
            }

        })


    

}
function sceneshare(){


    var sharelink = `${window.origin}/r/${_sid}`
    
    if(typeof _smartlink !== "undefined" && _smartlink !== null ){
        if( _smartlink.length >= 10)
        sharelink = `${window.origin}/r/${_smartlink}`
    }

    if(_default_lang_code != ''){
        sharelink += `?lang=${_default_lang_code}`;
    }
    
	text = `<div class="form-group">
	<label for="sceneid" class="text-white font-weight-light"></label>
	<input type="text" class="form-control validate" id="shareurl" value='${sharelink}' required>
	</div>`

	var dialog = bootbox.dialog({
		title    : 'Share',
		message  : text,
		size     : 'medium',
		backdrop : false,
		onEscape : false,
		buttons  : {
			cancel  : {
				label     : 'Dismiss',
				className : 'btn btn-secondary',
				callback  : function() {
					console.log('Custom cancel clicked');
				}
			},
			preview : {
				label     : 'Copy',
				className : 'btn btn-primary',
				callback  : function() {
					console.log("copy")
					sharelink = $("#shareurl").val().trim()

					console.log(sharelink)					
					ct.copylink("shareurl")
					return false;
				}
			},
			ok      : {
				label     : 'Go',
				className : 'btn btn-primary',
				callback  : function() {
					var win = window.open(sharelink, '_blank');
					win.focus();

					return false;
				}
			}
		}
    });
  
}

function copysharelink() {
    sharelink = $('#sharelink').val().trim();

    console.log(sharelink);

    ct.copylink('sharelink');
}

function gosharelink() {
    sharelink = $('#sharelink').val().trim();

    console.log(sharelink);

    //window.location.replace(sharelink)

    var win = window.open(sharelink, '_blank');
    win.focus();
}

function addbaseClass(){
    /*********** Add class ***********/
    console.log("addbaseClass")
    return new Promise(function (resolve, reject) {
        console.log(_config_param)
        console.log(_SubModule)

        if(typeof _SubModule !== "undefined"){
            if(_SubModule.indexOf("navigation") !== -1){
                _submodule_check = true;
            }
        }

        if(_scenetype == "3dnavigation" || _submodule_check){
          
            if(_isinvtag){
                addinvtag().then((result)=> {
                    return resolve(true);
                });
            }else{
                return resolve(true);
            }
        }else{
            var elemenu_setting = new category_setting(_config_param);
            elemenu_setting.cat_ready();
            elesetting = new ele_setting(true,true,true);
            elesetting.load_setting().then((result)=> {
                console.log("elesetting end")
                console.log(_eleSetting)
                console.log("_isinvtag")
                console.log(_isinvtag)
                if(_isinvtag){
                    addinvtag().then((result)=> {
                        return resolve(true);
                    });
                }else{
                    return resolve(true);
                }
            });
            console.log('add class')
            console.log(_mpSdk)
        }
    });
   

}

function addinvtag(){
    return new Promise(function (resolve, reject) {
        var in_tag = new invisible_tag(_mpSdk);
        in_tag.invisible_tag_init().then((result)=> {
            console.log("addinvtag")
            in_tag.invisible_tag_mapchange().then((result)=> {
                console.log("addinvtag end") 
                return resolve(true);
            });
            
        });
    });
}

function handleScene() {

        console.log(_isDevelopmentMode)
        if (_isDevelopmentMode){


            var param = {};
            param.action = "checkcountryWithSid"
            param.module = "generic"
            param.sid = _sid;
           

            ct.api(param)
            .then((e)=>{

                console.log(e)

            })





            var hkdiv = `
        
            <style>
            #countrydiv{
                position:absolute;
                left:10px;
                bottom:80px;
                background-color:white;
                width:70px;
                height:30px;
                z-index:999999;
                display:none;
            }

            </style>

            <div id=countrydiv></div>
            `

            $("#container").append(hkdiv)

        

        }


        var str = "Non CN"
    
        if (isChinaIP){
            str = "CN"
        }

        $("#countrydiv").html(str);


        if (ct.getUrlParameter("hk") == "1"){
            getMid(false)
        }
        else{
            getMid(isChinaIP);
        }
       
}

function getMid(isChina) {

    if (_sid) {

        console.log(_sid);

        var iframe = document.getElementById('showcase_iframe');
        iframe.addEventListener('load', showcaseLoader, true);
       
        ct.api( { "action": "getSid", "caseid": _sid, "isStat": !_isEditMode, "scenetype": _stype,"module":"scene" }).then((obj) => {
        var dt = Date.now();
        var path = _vn_asset_path+'scene/'+_sid+'.txt?t='+dt;
        //checkURL(path, function(status){
            
            //console.log(obj);
  
            if (!obj.error) {
            //if(status == 200 || status == 0){
                /*$.get(path,(data)=>{
                    obj = jQuery.parseJSON(ct.htmlspecialdecode(data));
                    _scene = jQuery.parseJSON(ct.htmlspecialdecode(data));
                    console.log(data);*/
                    _scene = obj;

                    
               
                    _isPaid = obj.ispaid;
                    if(_scene.isUserPremium == 1){
                        _isPaid = 1;
                    }

                    if (!_isEditMode){

                        console.log(obj.isRedirect)

                        if (obj.isRedirect == 1 && obj.redirectLink.length > 5){

                            var rurl = obj.redirectLink;

                            window.location.href =  rurl
                            return;

                        }


                    }


                    
                    if(_isPaid == false && _isfree == 0){
                        //var createedate = new Date(1000*_scene.createtime).getTime();
                        var todate = Math.round(new Date().getTime()/ 1000);
                        var Difference_In_Time = todate - _sub_expiry_date;
                        var Difference_In_Days = parseInt(Difference_In_Time / (3600 * 24));
                        if(Difference_In_Days >= 0){
                            
                            if(Difference_In_Days > _config_param.scene_probation_day){
                                if(_isEditMode){
                                    window.location.replace(window.location.origin+"/project");
                                }else{
                                    window.close();
                                }
                            }else{
                                _isPaid = 1;
                            }
                        }  
                    }
                    _bgmId = obj.isBgm;

                    _mpid = obj.matterportid;
                    _isBundle = obj.isBundle;
                    _is_full_btn = obj.isshowfullscreen;
                    _is_bundle_show_tag = obj.isshowbundletag;
                    _isbundlelight = obj.isbundlelight;
                    _button_bg_color = obj.button_bg_color;
                    _button_fa_color = obj.button_fa_color;
                    _smartlink = obj.smartlink;
                    _is3dscale = obj.is3dScale;
                    _default3dlight = obj.default3dlight;

                    isShowMTagListBtn = obj.isShowMTagListBtn;
                    _isShare = obj.isShare;
                    _is360 = obj.is360;
                    bulid_share_btn();

                    
                    if(typeof ct.getUrlParameter('iscancelBundle') !== 'undefined' && ct.getUrlParameter('iscancelBundle') == 1){
                        _isBundle = '0';
                        _isSubBundle = false;
                    }

                    var btn_title_html = `<div class="btn_title">
                    <p class="title">${_scene.title} - ${_scene.scenename}</p>
                    <p class="sub_title">Project ID : ${_sid} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Matterport ID : ${_scene.matterportid}`;
                    if(_isEditMode){
                        console.log('_scene.paidExpiry67');
                        console.log(_scene.paidExpiry);
                        console.log(_scene);
                        if(_scene.paidExpiry !== "0"){
                            var months_arr = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ];
                            var expires = new Date(1000*_scene.paidExpiry),
                            expires_month = '' + months_arr[expires.getMonth()],
                            expires_day = '' + expires.getDate(),
                            expires_year = expires.getFullYear();
                            btn_title_html += `&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plan Expires : ${expires_day+" "+expires_month+" "+expires_year}`;
                        }
                        console.log(_scene.ispaid);
                        console.log(_scene.isUserPremium);
                        if(_scene.ispaid == false && _scene.isUserPremium == 0){
                            btn_title_html += `&nbsp;&nbsp;<button id=payment style="height: 17px;
                            width: 97px;
                            background-color: #68c8c8;
                            border-radius: 9px;border:0;color:#fff;">Renew Plan</button>`;
                        
                        }
                    }
                    btn_title_html += `</p></div>`;
                    
                    if(typeof ct.getUrlParameter('hide') != 'undefined'){
                        if(ct.getUrlParameter('hide') !== "1"){
                            btnlist.append(btn_title_html);
                        }
                    
                    }else{
                        btnlist.append(btn_title_html);
                    
                    }

                    if($("#payment").length > 0){
                        $("#payment").on("click", function () {
                            payment();
                        })
                    }
                
                    if(!_isEditMode){
                        //$('.btn_title').css({'left':'auto'});
                    
                        //$('#buttonlist').css({'background-image': 'linear-gradient(rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0))',"background-color":"rgba(0,0,0,0)"});
                    }else{
                        $('.btn_title').show();
                        $('#buttonlist').css({'background-image': 'url("")',"background-color":"#ffffff"});
                    }
                    
                    if(_bgmId !== null && typeof _bgmId != 'undefined'){
                    
                        console.log(_bgmId)
                        onYouTubeIframeAPIReady_s(_bgmId);
                    
                        /*$('body').append(`<div style='position: absolute;bottom: 3.8%;right: 5%;'>
                        <input id='volume' class='main_color' type="range" min="0" max="100" value="50"  ></input>
                    
                        </div>`);

                        $("#volume").on('change', function() {
                            setBGMVolume($(this).val());
                        });*/
                    }
                    _scenetype = obj.scenetypecode;
                    _default_lang_code = obj.default_lang_code;
                    if(obj.cus_title == "NULL" || obj.cus_title == null || obj.cus_title == ''){
                        document.title = obj.title;
                    }else{
                        document.title = obj.cus_title;
                    }

                    $.grep(_scenetype_list, function (element, index) {
                        if (element.code == _scenetype) {
                            _ispayment = element.ispayment;
                            _isTagList = element.isTagList;
                            _isStartBtn = element.isStartBtn;
                            _isShareBtn = element.isShareBtn;
                            _isvr = element.is_vr;

                            console.log(element);

                            //if (_isTagList == 1) {
                            if( isShowMTagListBtn == 1) {

                                $("#taglist").css("display", "block");   
                            }
                            /*} else {
                                $("#taglist").css("display", "none");
                            }*/

                            //if (_isShareBtn == 1) {
                                $("#_shareBtn").css("display", "block"); 
                            /*} else {
                                $("#_shareBtn").css("display", "none");
                            }*/

                            if (_isStartBtn == 1 && _isEditMode && _scenetype != 'syncchat') {

                                $("#startBtn").css("display", "block");

                            }
                            else {
                                $("#startBtn").css("display", "none");
                            }
                        }
                    });

                    if (obj.scenetypecode == _stype || (_stype.substr(0, 8) == "syncmove" && obj.scenetypecode.substr(0, 8) == "syncmove") || (_stype.substr(0, 8) == "syncChat" && obj.scenetypecode.substr(0, 8) == "syncchat")) {

                        updateSceneData(_sid);


                        var url = obj.matterporturl;

                        _murl = obj.matterporturl;

                        _newmpid = obj.matterportid;
                        _murlparameter = obj.urlparameter;
                        if(_isBundle == '1'){



                            var bundle_dir = "mpbundle";

                            console.log(isChina)
                            console.log(obj.isChina)
                            
                            if(_is_bundle_show_tag == 1){




                                url = url.replace("https://my.matterport.com/show/?m=", `./${bundle_dir}/showcase.html?m=`);
                                url += '&applicationKey=e2d25a1dd1254030a411ba1401b9c1f2';
                            }else{
                                url = url.replace("https://my.matterport.com/show/?m=", `./${bundle_dir}/showcase_notag.html?m=`);
                                url = url.replace(`./${bundle_dir}/showcase.html?m=`, `./${bundle_dir}/showcase_notag.html?m=`);
                                url += '&applicationKey=e2d25a1dd1254030a411ba1401b9c1f2';
                        
                            }

                            if (obj.isChina){

                                url = url.replace(`showcase`,`showcase_cn_notag`);
                            }

                            
                            if ( window.location !== window.parent.location ) {

                                console.log("anserorigin exist")

                                console.log(document.referrer);

                                
                                const urls = new URL(document.referrer);

                                console.log(urls);

                                var host = urls.host
                                console.log(host)
                                if (host == "l2vrexperience.com" || host == "divevirtual.com" || host == "vzit.webspark.dev" ){
                                    url = url.replace("bundle","bundle_cors");

                                }
                                
                            }
                            else{
                            
                                console.log("no ancestor origin")
                            }


                            console.log(url)
                            
                        }else{
                            url = "https://my.matterport.com/show/?m="+_newmpid+_murlparameter;
                            if (obj.isChina){

                                url = url.replace("my.matterport.com","my.matterportvr.cn");
                            }
                        }

                        console.log(url)
                        console.log("final url");
                        if(!_isEditMode && _ispremium == 0 && _isPaid !== 1){
                            iframe.remove();
                            bootbox.confirm({
                                message: `${_config_param.scene_expiry_alert}`,
                                buttons: {
                                    confirm: {
                                        label: 'Yes',
                                        className: 'btn-success'
                                    },
                                    cancel: {
                                        label: 'No',
                                        className: 'btn-danger'
                                    }
                                },
                                callback: function (result) {
                                    window.location.replace("main");
                                }
                            });
                        
                        }
                        iframe.src = url;

                        if (ct.getUrlParameter('paypal')) {

                            console.log("paypal")
                            if (ct.getUrlParameter('paypal') == 1) {
                                _authkey = ct.getCookie("authkey");

                                if (_authkey) {

                                    if (obj.ispaid) {

                                        bootbox.alert({
                                            message: "Your payment was completed. Thanks for your business! Enjoy your premium journey now.",
                                            callback: function () {
                                                search = window.location.search
                                                console.log(search);
                                                str = ""
                                                console.log(search.substr(0, 1))
                                                if (search.substr(0, 1) == "?") {

                                                    ss = search.substr(1);

                                                    ssArray = ss.split("&")

                                                    for (i = 0; i < ssArray.length; i++) {

                                                        parm = ssArray[i];

                                                        if (parm.substr(0, 6).toUpperCase() != "PAYPAL") {
                                                            str += parm + "&"
                                                        }

                                                    }
                                                }

                                                console.log(str.substr(0, str.length - 1))
                                                window.location.replace(`${window.location.origin}/scene?${str.substr(0, str.length - 1)}`);

                                            }
                                        })
                                    }

                                }
                            }
                        }
                    }

                    module_init()   // module init

                    if (_isSubModule){
                        submodule_init();
                    }

                    if(_isSubBundle){

                        if (typeof subbundle_init === "function"){
                            subbundle_init();
                        }
                        
                    }
                //});
            }
            else {
                bootbox.alert(obj.description);
                return;
            }
        });
    }
    
}


function updateSceneData(sid) {

    ct.api( { "action": "refreshSceneData", "sid": _sid,"module":"scene" }).then((obj) => {



        console.log(obj);
    })


    if (!_isEditMode) {

        $("#backBtn").css("display", "none")
    }
    else {
        $("#backBtn").css("display", "block");
    }

    if(_stype == "syncmove_host"){
        $("#backBtn").css("display", "none")
    }
    console.log("_isPaid")
    console.log(_ispremium)
    console.log(_isPaid)
    console.log(_ispayment)
    if (_isPaid == false && _isEditMode && _ispremium == 0) {
  
        //$("#payment").css("display", "block");
        console.log("slow renew plan")
        
    } else {
        $("#payment").css("display", "none");
    }


    if (_isTagList == 1  && _scene.isShowMTagListBtn == 1) {
        $("#taglist").css("display", "block");   
    } else {
        $("#taglist").css("display", "none");
    }



}



function showcaseLoader() {
    var key = 'e2d25a1dd1254030a411ba1401b9c1f2';
    console.log("finish iframe loaded");
    var iframe = document.getElementById('showcase_iframe');
    //console.log(window.MP_SDK)
    if(_isBundle == '1'){
        iframe.contentWindow.MP_SDK.connect(iframe, key, '3.4').then(function (mpSdk) {
            _mpSdk = mpSdk
            if(!_isbasefunctionloaded){
                sweep_controll().then((result)=> {
                    loadedShowcaseHandler(mpSdk)
                    console.log("sweep_controll end")
                   
                });
            }
            
            ele3d.ele3d_ready();
           
        });
    }else{
        
        window.MP_SDK.connect(
            document.getElementById('showcase_iframe'),
            'e2d25a1dd1254030a411ba1401b9c1f2', // SDK Key -- swap for your own.
            '3.13' // SDK version
        )
        .then(function (mpSdk) {
            _mpSdk = mpSdk
            console.log("Embed")
            if(!_isbasefunctionloaded){
                sweep_controll().then((result)=> {
                    loadedShowcaseHandler(mpSdk)
                    console.log("sweep_controll end")
                
                });
            }
        
           
            
        })
        .catch(function (e) {
            console.log(e)
            //bootbox.alert("Ooops. Something goes wrong, please try later");
        });
    }
    
    



}



function loadedShowcaseHandler(mpSdk) {

    console.log("loadedshowcasehandlder");

    if (typeof sdkReady === "function"){
        sdkReady();
    }
    
    if (_isSubModule){
        if (typeof sub_sdkReady === "function"){
            sub_sdkReady();
        }
    }

    if(_isSubBundle){
        subb_sdkReady();
    }

    if (!_isEditMode){
        if (typeof startsubscribe === "function" ){


            if (!_stype.match(/navigation/i)){

                startsubscribe();
            }

            
        }


    }

    
    if(_isvr == "1" && /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))){
        if(_is360 == "1"){
            var vr = new vr_mode();
            vr.setting();
        }
       
        
    }

  
    mpSdk.App.state.subscribe(function (appState){
        // app state has changed
        console.log('The current application: ', appState.application);
        console.log('The current phase: ', appState.phase);
        console.log('Loaded at time ', appState.phaseTimes[mpSdk.App.Phase.LOADING]);
        console.log('Started at time ', appState.phaseTimes[mpSdk.App.Phase.STARTING]);

        if (appState.phase == 'appphase.playing') {

            build_minimap_btn();
           
            if(_isBundle == '0'){
                
                console.log("mapphasechange")
                mapphasechange(appState)

                if (_isSubModule){
                    console.log("sub_mapphasechange")
                    sub_mapphasechange(appState);
                }
                
                
            }else if(_isBundle == '1'){
               
                
                mapphasechange(appState)
                if (_isSubModule){
                    sub_mapphasechange(appState);
                }
                if(_isSubBundle){
                    subb_mapphasechange(appState);
                }
                  
               

            }
            // check whether start position exists ...
            if (ct.getUrlParameter('sp') && !_isEditMode) {

                var param = {};

                param.action = "getStartPosition";
                param.module = "scene"
                param.sid = _sid;



                param.sp = ct.getUrlParameter('sp');

                ct.api(param)
                .then((res) => {
                    console.log("start pos");
                    console.log(res);

                    var startdata = res.startdata;

                    _mpSdk.Sweep.moveTo(startdata.sweep, {
                        rotation: startdata.rotation,
                        transition: _mpSdk.Sweep.Transition.INSTANT
                    }).then((sweepId) => {
                        console.log("finish");
                    })



                })
            }
            if(!_isMobile && !_isEditMode && _is_full_btn == '1'){
                var fullsrceen_btn = `<button type="button" class="full_btn" title="Fullscreen">
                                <span class="icon icon-share"><i class="fas fa-expand"></i></span>
                            </button>`;
                $('body').append(fullsrceen_btn);
                $('.full_btn').click(function(){
                    if($(this).find('.icon').html() == '<i class="fas fa-expand"></i>'){
                        openFullscreen();
                    }else{
                        closeFullscreen();
                    }
                });
            }
            
            document.title = ct.htmlspecialdecode($('title').text());  
            
        }
            _mpSdk.Mattertag.getData()
            .then((e) => {
                var ecount = 0;
                console.log(e);
     
                if(e.length == 0){
                    $("#taglist").css("display", "none");
                }else{
                    e.forEach(element => {
                        if (element['enabled']) {
                            ecount ++;
                        }
                    });
                    if(ecount == 0){
                        $("#taglist").css("display", "none");
                    }
                   
                }
                _mattertags = e;
                get_tagicon();
                if(_isBundle == '1'){
                    if(_is_bundle_show_tag !== '1'){
                        var bundle_tag_setting = new ele_bundle_tag();
                        bundle_tag_setting.bundle_tag_int();
                    }
                }
                
            })
    
    })



    mpSdk.Camera.zoom.subscribe((zoom)=>{

        _zoom = zoom.level;


        if (typeof mapzoom !== "undefined"){

            mapzoom(_zoom)
        }

    })



    mpSdk.Camera.pose.subscribe(function (e) {

        //console.log("subscribe 1");
        _lastsweep = e.sweep;
        _lastposit = e.position;
        _lastRotation = e.rotation;
        _lastmode = e.mode;
        _pose = e;

        //console.log(_lastRotation);
        mapmove(e)

        if (_isSubModule){
            sub_mapmove(e)
        }
        if(_isSubBundle){
            subb_mapmove(e)
        }
    });



    mpSdk.Camera.pose.subscribe(function (e) {
        //console.log("subscribe 2")
        //console.log(e)
    });
   
    mpSdk.Model.getData()
        .then(function (e) {



            console.log(e)
          
            _mpid = e.sid;
    

            console.log(_allSweepArray)

        })
   
    console.log("1111");
    var iframe = $('#showcase_iframe'); 
    console.log(iframe);
    console.log($('.collapsible-list-items', iframe.contents()).length);
    var btn = `<div role="button" aria-label="Measurement Mode" aria-disabled="false" class="icon-button button-interactive a1111"><div role="button" data-balloon="Measurement Mode" data-balloon-pos="up" class="data-balloon"><span class="icon icon-tape-measure"></span></div></div>`;
    // $('.collapsible-list-items', iframe.contents()).prepend(btn);
    // $('.a1111', iframe.contents()).click(function(){
    //     console.log("a1111")
    // });
        $('#buttonlist button').removeClass('main_color');
        $('#buttonlist button').css({'background-color': `${_button_bg_color}`});
        $('#buttonlist button i').removeClass('fa_color');
        console.log("button_fa_color")
        console.log(_button_fa_color)
        console.log(_button_bg_color)
        $('#buttonlist button i').css({'color': `${_button_fa_color}`});
        // on(event: Mode.Event.CHANGE_START, callback: function): void
        // on(event: Mode.Event.CHANGE_END, callback: function): void
      
}



/// payment 

function payment() {
    /*var param = {};
    param.sid = _sid;
    param.module = "payment";
    param.action = "auth_scene_type_payment_info";
    console.log(_sid);
    ct.api(param).then((result) => {
        result.description = result.errPaidMsg
        console.log(result);
        main_promptpayment(result);
    });*/
    console.log('pay')
    window.location.replace("../pricing");
}

function sweep_controll(){
    return new Promise(function (resolve, reject) {
        console.log("sweep_controll start")
        _mpSdk.Sweep.data.subscribe({
            onAdded: function (index, item, collection) {
            //console.log('sweep added to the collection', index, item, collection);
            _allSweepArray.push(item);
            },
            onRemoved: function (index, item, collection) {
            //console.log('sweep removed from the collection', index, item, collection);
            },
            onUpdated: function (index, item, collection) {
            //console.log('sweep updated in place in the collection', index, item, collection);
            },
            onCollectionUpdated: function (collection) {
                if(!_isbasefunctionloaded){
                    _isbasefunctionloaded = true;
                    console.log('the entire up-to-date collection', collection);
                    if(_scenetype !== "categories"){
                        if(typeof ct.getUrlParameter('hide') == 'undefined'){
                            if(_isbaseClass){
                                addbaseClass().then((result)=> {
                                    return resolve(true);
                                });
                            }else{
                                if(_isinvtag){
                                    addinvtag().then((result)=> {
                                        return resolve(true);
                                    });
                                }else{
                                    return resolve(true);
                                }
                            }
                            
                        }else{
                            return resolve(true);
                        }
                    }else{
                        return resolve(true);
                    }
                }
            }
        });
    });
}


function main_promptpayment(obj) {

    console.log("main payment")
    console.log(_userId);
    console.log(obj)
    if(parseInt(_userbalance) >= parseInt(obj.premiumfee)){
        label = 'Upgrade by balance ( '+  _userbalance + ' )';
    }else{
        label = 'Upgrade by Paypal';
    }
    if(typeof obj.description !== 'undefined' && obj.description.length > 0){
        if($('.bootbox-confirm').length == 0){
            bootbox.confirm({
                message: '<p>'+obj.description+'</p>',
                buttons: {
                    confirm: {
                        label: label,
                        className: 'btn-primary'
                    },
                    cancel: {
                        label: 'No,thanks',
                        className: 'btn-secondary'
                    }
                },
                callback: function (result) {
                    console.log('This was logged in the callback: ' + result);
        
                    if (result) {
                        if(parseInt(_userbalance) >= parseInt(obj.premiumfee)){
                            redirectToBalance(obj);
                        }else{
                            redirectToPayPal(obj)
                        }
                        
        
                    }
                }
            });
            $('.bootbox-body p').css({'font-family': 'SFProDisplay-Bold','font-size': '12px','color': 'rgba(40, 40, 40, 1)'});
        }
        
    }
    


}

function redirectToBalance(obj){
    var param = {};
    param.action = 'auth_payBalance'
    param.userid = _userId;
    param.sid = _sid;
    param.module = "payment";
    param.premiumfee = obj.premiumfee;
    
    ct.api(param).then((result) => {
        if(!result.error){
            var newbalance = parseInt(_userbalance) - parseInt(obj.premiumfee);
            var msg = "US "+ obj.premiumfee +". has been deducted. The new balance is US "+ newbalance +" . Thanks for your purchase."
            bootbox.alert(msg, function(){ 
                location.reload();
            });
        }
    });
   
}



function redirectToPayPal(obj) {

    path = "https://www.sandbox.paypal.com/cgi-bin/webscr";

    if (_isProduction ) {

        path = "https://www.paypal.com/cgi-bin/webscr";
    }


    para = {
        "redirect_cmd": "_xclick",
        "cbt": "www.l2vr.co",
        "cmd": "_ext-enter",
        "business": "onlinepayment@l2vr.co",
        "amount": obj.premiumfee,
        "no_note": 1,
        "no_shipping": 1,
        "item_name": "Pay as U go",
        "notify_url": `${window.location.origin}/paypal_receive.php`,
        "custom": `scene_${_sid}`,
        "currency_code": "USD",
        "country": "US",
        "lc": "GB",
        "return": `${window.location.href}&paypal=1`

    }



    
    // para = {
    //     "redirect_cmd": "_xclick-subscriptions",
    //     "cbt": "www.l2vr.co",
    //     "cmd": "_xclick-subscriptions",
    //     "business": "onlinepayment@l2vr.co",
    //     "a3": 5,
    //     "no_note": 1,
    //     "no_shipping": 1,
    //     "item_name": "Ugrade to Premium Monthly",
    //     "notify_url": `${window.location.origin}/paypal_receive.php`,
    //     "custom": `scene_${_sid}`,
    //     "currency_code": "USD",
    //     "country": "US",
    //     "lc": "US",
    //     "p3" : 1,
    //     "t3" : "D",
    //     "src":1,
    //     "return": `${window.location.href}&paypal=1`

    // }

    console.log(para);


    var form = $('<form></form>');

    form.attr("method", "post");
    form.attr("action", path);

    $.each(para, function (key, value) {
        var field = $('<input></input>');

        console.log(key, value)

        field.attr("type", "hidden");
        field.attr("name", key);
        field.attr("value", value);

        form.append(field);
    });

    // The form needs to be a part of the document in
    // order for us to be able to submit it.
    $(document.body).append(form);

    console.log(form)

    form.submit();


}

function get_hostname(url) {
    var m = url.match(/^http:\/\/[^/]+/);
    return m ? m[0] : null;
}
////////////////YOUTUBE BGM/////////////////

function loadScript() {
    if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }
}

function onYouTubeIframeAPIReady_s(videoId) {
    console.log("scene utube");
    console.log(videoId);
    if(videoId != null && typeof videoId != 'undefined' && typeof YT !== 'undefined'){
        player = new YT.Player('bgm', {
            height: '0',
            width: '0',
            videoId: videoId,
            playerVars:{
                autoplay:0,
                loop: 1,
            },
            events: {
            'onReady': onPlayerReady,
            'onStateChange': 
              function(e) {
                  if (e.data === YT.PlayerState.ENDED) {
                      player.playVideo(); 
                  }
              }
            }
        });
    }
}

function onPlayerReady(event) {
    /*event.target.playVideo();
    event.target.setVolume(50);
    event.target.setLoop(true);*/
}

function playBGM_youtube(){
    player.playVideo();
}

function pauesBGM_youtube(){
    player.pauseVideo();
}

function stopBGM_youtube(){
    player.stopVideo();
}

function setBGMVolume(vol){
    player.setVolume(parseInt(vol));
}

function openFullscreen() {
    var elem = document.documentElement;
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) { /* Safari */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE11 */
        elem.msRequestFullscreen();
    }
}

function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.webkitExitFullscreen) { /* Safari */
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE11 */
        document.msExitFullscreen();
    }
}

function hide_full_btn(){
    _is_full_btn = 0;
}

function get_scene_info(){
    return new Promise((resolve, reject) => {
        console.log("get_scene_info");
        console.log(tc);
        console.log(sm);
        console.log(sb);
        //ct.api( { "action": "getSceneinfo", "sid": ct.getUrlParameter("sid"), "module":"scene" }).then((obj) => {
            console.log("get_scene_info api");
            //console.log(obj) 
            _stype = tc;
            if (sm.length > 0){
                _isSubModule = true;
                _SubModule = sm;
                
                if(_SubModule == 'room3' ||  _SubModule == 'burberry' || _SubModule == 'LCxGH' || _SubModule == 'DnG'){
                    if(!ct.getUrlParameter("edit")){
                        $('#buttonlist').hide();
                    }
                    
                }
            }

            if (sb.length > 0){
                _isSubBundle = true;
            }
            resolve(true);
        //});
    });
}

function s_getBase64(file = null) {
    return new Promise(function (resolve, reject) {
        if(file == null){
            resolve(null);
        }else{
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () { return resolve(reader.result); };
            reader.onerror = function (error) { return reject(error); };
        }
        
    });
};
