var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
function degrees_to_radians(degrees)
{
  var pi = Math.PI;
  return degrees * (pi/180);
}

var nodeObjWalking;
var urlObjWalking;
var posWalkingX =  -1.817106880555492,
    posWalkingY = 0.05138018727302551, 
    posWalkingZ = -1.208665578842865;

var Walking = /** @class */ (function () {
    function Walking(path, url) {
        var _this = this;
        this.isStop = false;
        this.filename = url;
        urlObjWalking = url;
        this.navpath = new Navigate3d(path, 0.5);
        this.chaturl = '';
        walk = this;
        this.walkmodels = [];
        this.speed = 200;
        this.numberOfModels = 1;
        this.path = path;
        this.modelposition = { x: posWalkingX, y: posWalkingY , z: posWalkingZ };
        this.modelrotation = { _x: 0, _y: 0, _z: 0, _order: 'XYZ' };
        this.modelscale = 0.8;
        this.mixers = [];
        this.initChatIframe();
        Promise.all([
            sdk.Scene.register('animation_walk', this.animationFactory)
        ]).
            then(function () {
            console.log("animation register done");
            for (var i = 0; i < _this.numberOfModels; i++) {
                _this.loadmodel();
            }
        });
    }
    Walking.prototype.initChatIframe = function () {
        // var _this = this;
        // var node = document.createElement('div');
        // var html = "\n        \n        <style>\n            #chatiframe{\n\n                position:relative;\n\n                width:100%;\n                height:90%;\n                \n                margin: 0 auto;\n                display:inline-block;\n\n\n            }\n\n\n            #closeChatIframeBtn{\n\n                position:relative;\n                float:right;\n                padding:5px;\n                //background-color:grey;\n                width:40px;\n                height:40px;\n                color:white;\n                display:inline-block;\n\n\n            }\n            #chatiframewrapper{\n                position:absolute;\n                width:80%;\n                height:80%;\n                top:50%;\n                left:10px;\n                transform:translate(0%,-50%);\n                background-color:rgba(0,0,0,0.5);\n                display:none;\n                z-index:9999999;\n                max-width:400px;\n            }\n\n\n        </style>\n        \n            <div id=chatiframewrapper>\n            <div id=closeChatIframeBtn>X</div>\n            <iframe id=chatiframe src=" + this.chaturl + "></iframe>\n            \n            </div>\n\n        ";
        // node.innerHTML = html;
        // document.body.appendChild(node);
        // $("#closeChatIframeBtn").click(function (e) {
        //     _this.closeChatIframe();
        // });
    };
    Walking.prototype.loadmodel = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, sdk.Scene.createNode()
                            .then(function (node) {
                            node.obj3D.scale.set(_this.modelscale, _this.modelscale, _this.modelscale);
                            node.obj3D.position.set(_this.modelposition.x, _this.modelposition.y, _this.modelposition.z);
                            node.obj3D.rotation.set(0, Math.PI / 2, 0);
                            node.addComponent('animation_walk');
                            nodeObjWalking = node;
                            node.start();
                            console.log("model done");
                            console.log(node);
                            _this.walkmodels.push(node);
                            _this.startwalk(node);
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Walking.prototype.startwalk = function (node) {
        console.log("start walk");
        console.log(node);
        console.log(this.navpath);
        console.log(this.navpath.length);
        var currPos = node.obj3D.position;
        var nextPt = Math.floor(Math.random() * this.navpath.pathdots.length);
        console.log(nextPt);
        var nextpos = this.navpath.pathdots[nextPt].position;
        var route = this.navpath.planroute(currPos, nextpos);
        if (route.full.length > 10) {
            console.log(route);
            var adddotpath = Navigate3d.makeStepSmaller(route, 5);
            console.log(adddotpath);
            this.zombiemove(adddotpath, node, 0);
        }
        else {
            console.log("too short");
            this.startwalk(node);
        }
    };
    Walking.prototype.zombiemove = function (path, node, step) {
        // console.log(path,zombieid,step)
        var _this = this;
        var currstep = path[step];
        if (step < path.length - 5) {
            var nexstep = path[step + 5];
            var angle = Navigate3d.findRotateAngle(currstep, nexstep);
            // console.log(angle)
            angle.y += 180;
            node.obj3D.rotation.set(node.obj3D.rotation.x, degrees_to_radians(angle.y), node.obj3D.rotation.z);
        }
        //console.log(currstep)
        node.obj3D.position.set(currstep.x, currstep.y, currstep.z);
        if (!this.isStop) {
            if (step < (path.length - 2)) {
                setTimeout(function () {
                    step++;
                    _this.zombiemove(path, node, step);
                }, this.speed);
            }
            else {
                this.startwalk(node);
            }
        }
    };
    Walking.prototype.animationFactory = function () {
        return new animation_walk();
    };
    Walking.prototype.finishNotification = function () {
        console.log("finish component");
        console.log(nodeObjWalking.obj3D.position);
        posWalkingX = nodeObjWalking.obj3D.position.x;
        posWalkingY = nodeObjWalking.obj3D.position.y;
        posWalkingZ = nodeObjWalking.obj3D.position.z;

        nodeObjWalking.stop();
    };
    Walking.prototype.update = function () {
        if (!this.isStop) {
            requestAnimationFrame(this.update.bind(this));
            var dt_1 = this.clock.getDelta();
            this.mixers.forEach(function (mixer) {
                //console.log(mixer)
                mixer.update(dt_1);
            });
        }
    };
    Walking.prototype.openChatIframe = function () {
        $("#chatiframewrapper").css("display", "block");
    };
    Walking.prototype.closeChatIframe = function () {
        var _this = this;
        // $("#chatiframewrapper").css("display", "none");
        this.isStop = false;
        this.walkmodels.forEach(function (node) {
            _this.startwalk(node);
        });
        this.update();
    };
    return Walking;
}());
function animation_walk() {
    this.onInit = function () {
        var _a;
        var _this = this;
        var THREE = this.context.three;
        console.log(THREE);
        walk.clock = new THREE.Clock();
        var loader = new THREE.GLTFLoader();
        var filename = walk.filename;

        console.log("Url Object: ", filename);

        loader.load(urlObjWalking, function (fbx) {
            console.log("load finish");
            console.log("fbx done");
            console.log(fbx);
            var mixer = new THREE.AnimationMixer(fbx.scene);
            var walkanim = fbx.animations[0];
            var action = mixer.clipAction(walkanim);
            action.clampWhenFinished = true;
            console.log('Walk Anim: ', walkanim);
            walk.mixers.push(mixer);
            action.play();
            action.timeScale = 0.8;
            walk.update();
            _this.outputs.objectRoot = fbx.scene;
            _this.outputs.collider = fbx.scene;
            fbx.scene.traverse(function (child) {
                //console.log(child)
                if (child.material) {
                    child.material.metalness = 0;
                }
            });
        });
        this.events = (_a = {},
            _a["INTERACTION.CLICK"] = true,
            _a["INTERACTION.HOVER"] = true,
            _a);
        this.onEvent = function (type, data) {
            console.log('POSITION WALKING 3d object: ', nodeObjWalking.obj3D.position.x);
            if (type == "INTERACTION.CLICK") {
                walk.isStop = true;
                embedImageClicked(
                    nodeObjWalking.obj3D.position.x, 
                    nodeObjWalking.obj3D.position.y + 1.5, 
                    nodeObjWalking.obj3D.position.z, true);
                window.postMessage({
                    'message': 'OPEN IFRAME WALKING',
                }, '*');
                // walk.openChatIframe();
                setTimeout(()=>walk.closeChatIframe(), 3000);
            }
        };
    };
}
