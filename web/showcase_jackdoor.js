function listenContentLoadJackDoor(){
    const iframe = document.getElementById('showcase-iframe-jackdoor');
  
    if(iframe!=null){
        connectSdkJackDoor();
    }else{
        sleep(5000).then(() => { listenContentLoadJackDoor(); });
    }
  }
  
  function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
  }
  
  async function connectSdkJackDoor() {
    // connect the sdk; log an error and stop if there were any connection issues
    try {
        connectShowcaseJackDoor();
    } catch (e) {
        console.error(e);
    }
  }
  
  async function connectShowcaseJackDoor(){
    const showcase = document.getElementById('showcase-iframe-jackdoor');
    const showcaseWindow = await showcase.contentWindow;
    
    setTimeout(async function (){
        sdk = await showcaseWindow.MP_SDK.connect(showcaseWindow, 'gznrh4n499h4qhewiitm1hidc', '');
    
        console.log('SDK: ', sdk);
        initLight();
        var paramStart = '{"filename": "gif_dance-06.png", "scale": {"x": 1.5, "y": 2.5, "z": 1}, "position": {"x": 0, "y": 0.6394819720827437, "z": -0.019771834835410118}, "rotation":{"_x": 0, "_y":-99.136111684404, "_z": 0}, "url": "https://3d2api.discoverfeed.info/version-test/api/1.1/wf/push", "urlImage": "https://cdn.jsdelivr.net/gh/duchieupham/matterport@main/gif_dance-06.png", "id": "01", "message": "JACK", "holdonposyoffset": 1.6}';
        var paramEnd = '{"filename": "gif_dance-06.png", "scale": {"x": 1.5, "y": 2.5, "z": 1}, "position": {"x": 10.758977890014648, "y": 0.6394819720827437, "z": 0.062314484268426895}, "rotation":{"_x": 0, "_y":-99.136111684404, "_z": 0}, "url": "https://3d2api.discoverfeed.info/version-test/api/1.1/wf/push", "urlImage": "https://cdn.jsdelivr.net/gh/duchieupham/matterport@main/gif_dance-06.png", "id": "01", "message": "OCTAGON", "holdonposyoffset": 1.6}';
        
        initJackDoorLobby(paramEnd);
        initJackDoorLobby(paramStart);
    }, 1000);
  }

  function initJackDoorLobby(param){
    new Door(JSON.parse(param));
  }
  