var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var configPositionJackdoor = {};
var Door = /** @class */ (function () {
    function Door(param) {
        this.events = {};
        this.doorpath = "../vn_assets/subassets/octagon/jack_img/";
        this.param = param;
        console.log('PARAM : ', this.param);
        this.url = param.url;
        this.urlImg = param.urlImage;

        configPositionJackdoor.x = this.param.position.x;
        configPositionJackdoor.y = this.param.position.y + this.param.holdonposyoffset;
        configPositionJackdoor.z = this.param.position.z;

        jackdoor = this;
        console.log(param);
        this.register();
    }
    Door.prototype.register = function () {
        var _this = this;
        Promise.all([
            sdk.Scene.register('clickdoor', this.doorFactory)
        ]).
            then(function () {
            console.log("register done");
            _this.loadDoor();
        });
    };
    Door.prototype.updatepicture = function () {
        var material = this.node.obj3D.children[0].material;
        console.log(material);
        var path = "" + this.doorpath + this.param.filename;
        console.log(path);
        console.log(jackdoor.THREE);
        var texture = new jackdoor.THREE.TextureLoader().load(path);
        texture.minFilter = jackdoor.THREE.LinearFilter;
        material.map = texture;
        material.map.needsUpdate = true;
    };
    Door.prototype.loadDoor = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, sdk.Scene.createNode()
                            .then(function (node) {
                            var scale = _this.param.scale;
                            var position = _this.param.position;
                            var rotation = _this.param.rotation;
                            console.log('Position: ', position);
                            console.log('Rotation: ', rotation);
                            node.obj3D.scale.set(scale.x, scale.y, scale.z);
                            node.obj3D.position.set(position.x, position.y, position.z);
                            node.obj3D.rotation.set(rotation._x, rotation._y, rotation._z);
                            // node.obj3D.name = _this.param.url;
                            node.obj3D.name = _this.param.id;
                            console.log('Name object: ', node.obj3D.name);
                            node.addComponent('clickdoor');
                            _this.node = node;
                            // setTimeout(()=>{
                            // },1000)
                            console.log(node);
                            node.start();
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Door.prototype.doorFactory = function () {
        return new clickdoor();
    };
    Door.prototype.handleclick = function (obj) {
        var _this = this;
        if (parseInt(obj.name) > 0) {
            // new HoldOn(configPositionJackdoor);

            console.log(this.url);
            // var browserId = '';
            // if (ct.getUrlParameter('browserId')) {
            //     console.log("exist brosweri id");
            //     browserId = ct.getUrlParameter('browserId');
            // }
            console.log('Browser ID: ', browserId);
            var cb = {};
            cb.browserId = browserId;
            cb.doorID = obj.name;
            jackdoor.dispatch("notification", obj.position);
            $.post(this.url, cb, function (data) {
                console.log(data);
                console.log(obj.position);
                window.postMessage(
                    {
                        'message': _this.param.message
                    }, '*'
                )
            });
        }
    };
    ///////
    Door.prototype.dispatch = function (eventName, data) {
        var event = this.events[eventName];
        if (event) {
            event.fire(data);
        }
    };
    Door.prototype.on = function (eventName, callback) {
        var event = this.events[eventName];
        if (!event) {
            event = new DispatcherEvent(eventName);
            this.events[eventName] = event;
        }
        event.registerCallback(callback);
    };
    Door.prototype.off = function (eventName, callback) {
        var event = this.events[eventName];
        if (event) {
            delete this.events[eventName];
        }
    };
    return Door;
}());
///////
function clickdoor() {
    var _a;
    this.onInit = function () {
        var THREE = this.context.three;
        jackdoor.THREE = THREE;
        var geometry = new THREE.PlaneGeometry(1, 1);
        var path = jackdoor.urlImg;
        console.log(path);
        var texture = new THREE.TextureLoader().load(path);
        var material = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            side: THREE.DoubleSide,
            map: texture
        });
        material.map.needsUpdate = true;
        var tplane = new THREE.Mesh(geometry, material);
        this.outputs.objectRoot = tplane;
        this.outputs.collider = tplane;
    };
    this.events = (_a = {},
        _a["INTERACTION.CLICK"] = true,
        _a);
    this.onEvent = function (type, data) {
        console.log(type);
        console.log(data.collider);
        jackdoor.handleclick(data.collider.parent);
        embedImageClicked(configPositionJackdoor.x, configPositionJackdoor.y, configPositionJackdoor.z, true);
        //data.collider.parent.name
    };
}
