function changeAvatar(avatarId){
    console.log("This is a function from javascript code, avatar Id get from firestore: " + avatarId)
    var mainFrame  = document.getElementById("mtour")
   // mainFrame.style.visibility='hidden'
    // alert("Avatar change: " + avatarId)
    var param = {}
    param.avatar = avatarId
    mainFrame.contentWindow.postMessage(param, "*");
  
}

function copyText(url){
    navigator.clipboard.writeText(url);
}

function copyTextByExecCommand(url){
    try {
        document.execCommand("copy",false,url);
    } catch (error) {
        alert('Error ' + error)
    }
}