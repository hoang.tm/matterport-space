importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js");

firebase.initializeApp({
    apiKey: "AIzaSyD9-_Vync4-LMVlwHmHgsD-7quEWpD8bB8",
    authDomain:  "chat-app-eba66.firebaseapp.com",
    databaseURL: "https://react-native-firebase-testing.firebaseio.com",
    projectId: "chat-app-eba66",
    storageBucket: "chat-app-eba66.appspot.com",
    messagingSenderId: "369403714836",
    appId: "1:369403714836:web:0551a72f381f90c6543601",
});
// Necessary to receive background messages:
const messaging = firebase.messaging();

// Optional:
messaging.onBackgroundMessage((m) => {
    console.log("onBackgroundMessage", m);
});
